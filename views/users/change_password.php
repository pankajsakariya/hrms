<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\UserLevels;
use app\models\Center;
use app\models\States;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="users-form"  >

    <h2>Change Password</h2>
    <hr>

    
<?php 

if(isset($errorMessage) && $errorMessage!="")
{
    display_array($errorMessage);
}
?>
    
<?php $form = ActiveForm::begin(); ?>

    <div class="form-group field-users-mobile_number">
        <label class="control-label" for="users-mobile_number">Current Password</label>
        <input type="text" id="current_password" class="form-control" name="current_password" value="">
        <div class="help-block"></div>
    </div>
    
    <?php 
        $model->password = "";
        $model->confirm_password = "";
    ?> 
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'confirm_password')->passwordInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
