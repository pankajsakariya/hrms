<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
        
    
    
    <script>
        function submitm()
        {
            $("#form1").submit();
        }
    </script>
    
<?php
$form = yii\widgets\ActiveForm::begin([
           'action' => ['index'],
           'method' => 'get',
           'options' => [
                            'id' => 'form1'
                         ]
]);
?>
    
    <div class="row">
        <div class="form-group" style="float: right">
            <?= Html::submitButton('Search',['class' => 'btn btn-success']) ?>
        </div>
    </div>

    
    <div class="table-responsive">

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'header' => 'S No'
                , 'contentOptions' => ['style' => 'width: 60px;text-align: center;']],
            'id',
            'first_name',
            'last_name',
            'user_name',
            [
                'attribute' => 'user_level',
                'content' => function($data) {
                    $modelm = app\models\UserLevels::find()->where(["id" => $data->user_level])->one();
                    if ($modelm != null) {
                        return $modelm->level_name;
                    }
                }
                    ],
//                             [
//                        'attribute' => 'user_id',
//                        'header' => 'Center Type',
//                        'content' => function($data) {
//                            $modelm = app\models\Dependants::find()->where(["users_id" => $data->id])->one();
//                            if ($modelm != null) {
//                               // display_array($modelm->center_list);
//                                return centerArraytoData($modelm->center_list,"smart_center_type");
//                            } else {
//                                return "";
//                            }
//                        }
//                            ],
//                            [
//                        'attribute' => 'user_id',
//                        'header' => 'Area',
//                        'content' => function($data) {
//                            $modelm = app\models\Dependants::find()->where(["users_id" => $data->id])->one();
//                            if ($modelm != null && $modelm->center_list!="") {  
//                                return centerArraytoData($modelm->center_list,"center_name");
//                            } else {
//                                return "";
//                            }
//                        }
//                            ],
                    
//                                     [  
//                        'attribute' => 'user_id',
//                        'header' => 'City',
//                        'content' => function($data) {
//                            $modelm = app\models\Dependants::find()->where(["users_id" => $data->id])->one();
//                            if ($modelm != null && $modelm->district_list!="") {
//                                return centerArraytoData($modelm->district_list,"district_name");
//                            } else {
//                                return "";
//                            }
//                        }
//                            ],
                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]);
                    ?>

    
<?php yii\widgets\ActiveForm::end(); ?>
    
    
</div>
</div>