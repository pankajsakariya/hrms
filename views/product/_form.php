<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
if (!$model->isNewRecord && isset($model->color) && count($model->color) > 0) {
    $model->color = json_decode($model->color);
}
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rate_wholesale')->textInput() ?>

    <?= $form->field($model, 'rate_retail')->textInput() ?>

    <?= $form->field($model, 'color')->dropDownList((colorDropdown()), ['class' => 'form-control', 'multiple' => 'multiple']) ?>

    <?= $form->field($model, 'product_actual_weight')->textInput(["onkeypress" => "return isNumberKey(event)"]) ?>

    <?php echo $form->field($model, 'product_calculated_weight')->textInput(['readonly' => 'readonly','placeholder' =>'calculated from product raw material list']) ?>

    <?= $form->field($model, 'product_job_charge')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#product-product_actual_weight').on('change', function () {
            if ($(this).val() !== "") {
                $(this).val(parseFloat($(this).val()).toFixed(6));
            }
        });
    });
</script>