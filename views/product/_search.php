<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'product_name') ?>

    <?= $form->field($model, 'rate_wholesale') ?>

    <?= $form->field($model, 'rate_retail') ?>

    <?= $form->field($model, 'color') ?>

    <?php // echo $form->field($model, 'product_actual_weight') ?>

    <?php // echo $form->field($model, 'product_calculated_weight') ?>

    <?php // echo $form->field($model, 'product_job_charge') ?>

    <?php // echo $form->field($model, 'entry_by') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
