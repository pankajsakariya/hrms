<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RingAllocation */

$this->title = 'Create Ring Allocation';
$this->params['breadcrumbs'][] = ['label' => 'Ring Allocations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ring-allocation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
