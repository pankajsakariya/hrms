<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RingAllocation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ring-allocation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'program_detail_id')->textInput() ?>

    <?= $form->field($model, 'employee_id')->textInput() ?>

    <?= $form->field($model, 'ring_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ring_color')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_of_ring')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ring_weight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'on_date')->textInput() ?>

    <?= $form->field($model, 'entry_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_on')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
