<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RingAllocationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ring-allocation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'program_detail_id') ?>

    <?= $form->field($model, 'employee_id') ?>

    <?= $form->field($model, 'ring_type') ?>

    <?= $form->field($model, 'ring_color') ?>

    <?php // echo $form->field($model, 'no_of_ring') ?>

    <?php // echo $form->field($model, 'ring_weight') ?>

    <?php // echo $form->field($model, 'on_date') ?>

    <?php // echo $form->field($model, 'entry_by') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
