<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RingAllocationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ring Allocations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ring-allocation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ring Allocation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'program_detail_id',
            'employee_id',
            'ring_type',
            'ring_color',
            // 'no_of_ring',
            // 'ring_weight',
            // 'on_date',
            // 'entry_by',
            // 'created_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
