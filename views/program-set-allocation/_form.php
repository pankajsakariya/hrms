<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProgramSetAllocation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="program-set-allocation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'employee_id')->textInput() ?>

    <?= $form->field($model, 'program_detail_id')->textInput() ?>

    <?= $form->field($model, 'product_color')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'given_material_gross_weight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'number_of_product')->textInput() ?>

    <?= $form->field($model, 'ring_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_material')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'entry_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_on')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
