<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProgramSetAllocationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Program Set Allocations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-set-allocation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Program Set Allocation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'date',
            'employee_id',
            'program_detail_id',
            'product_color',
            // 'given_material_gross_weight',
            // 'number_of_product',
            // 'ring_status',
            // 'last_material',
            // 'entry_by',
            // 'created_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
