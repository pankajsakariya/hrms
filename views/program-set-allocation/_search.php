<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProgramSetAllocationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="program-set-allocation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'employee_id') ?>

    <?= $form->field($model, 'program_detail_id') ?>

    <?= $form->field($model, 'product_color') ?>

    <?php // echo $form->field($model, 'given_material_gross_weight') ?>

    <?php // echo $form->field($model, 'number_of_product') ?>

    <?php // echo $form->field($model, 'ring_status') ?>

    <?php // echo $form->field($model, 'last_material') ?>

    <?php // echo $form->field($model, 'entry_by') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
