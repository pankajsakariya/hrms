<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use \yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <!--<script src="web/webtheme/js/jquery.min.js"></script>-->

        <?php $this->head() ?>  

        <script src="<?php echo Url::to("@web/webtheme/js/bootstrap.min.js"); ?>"></script>

        <link href="<?php echo Url::to("@web/webtheme/css/style.css"); ?>" rel='stylesheet' type='text/css' >
        <link href="<?php echo Url::to("@web/webtheme/css/font-awesome.css"); ?>" rel="stylesheet"> 


        <!-- Mainly scripts -->
        <script src="<?php echo Url::to("@web/webtheme/js/jquery.metisMenu.js"); ?>"></script>
        <!-- Custom and pluginjavascript -->

        <link href="<?php echo Url::to("@web/webtheme/css/custom.css"); ?>" rel="stylesheet">
        <script src="<?php echo Url::to("@web/webtheme/js/custom.js"); ?>"></script>
        <style type="text/css">


            .dropdown-menu > li > a {
                white-space: normal !important;
            }
        </style>

    </head>
    <body>
        <?php $this->beginBody() ?>



        <!-- theme start -->

        <div id="wrapper">

            <!----->
            <nav class="navbar-default navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <h1> <a class="navbar-brand" href="<?php echo Url::to(['site/index']) ?>">Danone</a></h1>         
                </div>
                <div class=" border-bottom">
                    <div class="full-left">
                        <section class="full-top">
                            <!--<button id="toggle"><i class="fa fa-arrows-alt"></i></button>-->	
                        </section>

                        <div class="clearfix"> </div>
                    </div>


                    <!-- Brand and toggle get grouped for better mobile display -->

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="drop-men" >
                        <ul class=" nav_1">
                            
                            <li class="dropdown at-drop">
                                <a href="#" class="dropdown-toggle dropdown-at " data-toggle="dropdown"><i class="fa fa-globe"></i> <span class="number">5</span></a>
                                <ul class="dropdown-menu menu1 " role="menu">
                                    <li><a href="#">

                                            <div class="user-new">
                                                <p>New user registered</p>
                                                <span>40 seconds ago</span>
                                            </div>
                                            <div class="user-new-left">

                                                <i class="fa fa-user-plus"></i>
                                            </div>
                                            <div class="clearfix"> </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="user-new">
                                                <p>Someone special liked this</p>
                                                <span>3 minutes ago</span>
                                            </div>
                                            <div class="user-new-left">

                                                <i class="fa fa-heart"></i>
                                            </div>
                                            <div class="clearfix"> </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="user-new">
                                                <p>John cancelled the event</p>
                                                <span>4 hours ago</span>
                                            </div>
                                            <div class="user-new-left">

                                                <i class="fa fa-times"></i>
                                            </div>
                                            <div class="clearfix"> </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="user-new">
                                                <p>The server is status is stable</p>
                                                <span>yesterday at 08:30am</span>
                                            </div>
                                            <div class="user-new-left">

                                                <i class="fa fa-info"></i>
                                            </div>
                                            <div class="clearfix"> </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="user-new">
                                                <p>New comments waiting approval</p>
                                                <span>Last Week</span>
                                            </div>
                                            <div class="user-new-left">

                                                <i class="fa fa-rss"></i>
                                            </div>
                                            <div class="clearfix"> </div>
                                        </a></li>
                                    <li><a href="#" class="view">View all messages</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown"><span class=" name-caret">USER<i class="caret"></i></span><img src="<?php echo Url::to("@web/webtheme/images/setting.png"); ?>"></a>
                                <ul class="dropdown-menu " role="menu">
                                    <li><a href="profile.html"><i class="fa fa-user"></i>Edit Profile</a></li>
                                    <li><a href="inbox.html"><i class="fa fa-envelope"></i>Inbox</a></li>
                                    <li><a href="calendar.html"><i class="fa fa-calendar"></i>Calender</a></li>
                                    <li><a href="<?php echo Url::to(['/site/logout']) ?>"><i class="fa fa-power-off"></i>Logout</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                    <div class="clearfix"></div>

                    <div class="navbar-default sidebar" role="navigation">
                        <div class="sidebar-nav navbar-collapse">
                            <ul class="nav" id="side-menu">

                                <li>
                                    <a href="<?php echo Url::to(['/site/index']) ?>" class=" hvr-bounce-to-right"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Dashboards</span> </a>
                                </li>


                                <li class="active">
                                    <a href="#" class=" hvr-bounce-to-right text-primary"><i class="fa fa-desktop nav_icon"></i> <span class="nav-label">Forms</span><span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li><a href="<?php echo Url::to(['household-profile/index']) ?>" class=" hvr-bounce-to-right"> <i class="fa fa-circle-o-notch nav_icon"></i>Household Profile</a></li>
                                    </ul>
                                </li>





                                <li>
                                    <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-list nav_icon"></i> <span class="nav-label">Master Dropdowns</span><span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li><a href="<?php echo Url::to(['/agriculture-practice/index/']) ?>" class=" hvr-bounce-to-right"><i class="fa fa-circle-o-notch nav_icon"></i>Agriculture Practice</a></li>
                                        <li><a href="<?php echo Url::to(['/crop/index/']) ?>" class=" hvr-bounce-to-right"><i class="fa fa-circle-o-notch nav_icon"></i>Crop</a></li>
                                        <li><a href="<?php echo Url::to(['/education-qualification/index']) ?>" class=" hvr-bounce-to-right"><i class="fa fa-circle-o-notch nav_icon"></i>Education Qualification</a></li>
                                        <li><a href="<?php echo Url::to(['/primary-occupation/index']) ?>" class=" hvr-bounce-to-right"><i class="fa fa-circle-o-notch nav_icon"></i>Primary Occupation</a></li>
                                        <li><a href="<?php echo Url::to(['/type-of-phone/index']) ?>" class=" hvr-bounce-to-right"><i class="fa fa-circle-o-notch nav_icon"></i>Type of Phone</a></li>
                                        <li><a href="<?php echo Url::to(['/dairy-expense-dropdown/index']) ?>" class=" hvr-bounce-to-right"><i class="fa fa-circle-o-notch nav_icon"></i>Dairy Expense Source</a></li>
                                        <li><a href="<?php echo Url::to(['/dairy-expense-type/index']) ?>" class=" hvr-bounce-to-right"><i class="fa fa-circle-o-notch nav_icon"></i>Dairy Expense Type</a></li>
                                        <li><a href="<?php echo Url::to(['/village/index']) ?>" class=" hvr-bounce-to-right"><i class="fa fa-circle-o-notch nav_icon"></i>Village</a></li>
                                        <li><a href="<?php echo Url::to(['/financial-year/index']) ?>" class=" hvr-bounce-to-right"><i class="fa fa-circle-o-notch nav_icon"></i>Financial Year</a></li>
                                        <li><a href="<?php echo Url::to(['/units/index']) ?>" class=" hvr-bounce-to-right"><i class="fa fa-circle-o-notch nav_icon"></i>Units</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Reports & Graph</span><span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li><a href="#" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>Graphs</a></li>
                                        <li><a href="#" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>Reports</a></li>

                                    </ul>
                                </li>


                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="content-main">

                    <div class="grid-form">
                        <!--<div class="grid-form table-responsive" style="background-color: #fff;margin-left: 5px;margin-top: 5px;">-->
                        <div class="grid-form1 table-responsive">

                            <!--<div class="container">-->
                            <?=
                            Breadcrumbs::widget([
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            ])
                            ?>
                            <?= $content ?>

                            <!--</div>-->
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div style="clear:both"></div>
                </div>





                <!--                <div class="copy">
                                    <p> &copy; 2016 . All Rights Reserved | </p>
                                </div>-->
            </div>

            <div class="clearfix"> </div>
        </div>



        <!--  theme end  -->
        <script type="text/javascript">

//            $('.dropdown-toggle').on("click", function () {
//                var dropdownList = $('.dropdown-menu'),
//                        dropdownOffset = $(this).offset(),
//                        offsetLeft = dropdownOffset.left,
//                        dropdownWidth = dropdownList.width(),
//                        docWidth = $(window).width(),
//                        subDropdown = dropdownList.eq(1),
//                        subDropdownWidth = subDropdown.width(),
//                        isDropdownVisible = (offsetLeft + dropdownWidth <= docWidth),
//                        isSubDropdownVisible = (offsetLeft + dropdownWidth + subDropdownWidth <= docWidth);
//
//                if (!isDropdownVisible || !isSubDropdownVisible) {
//                    dropdownList.addClass('pull-right');
//                } else {
//                    dropdownList.removeClass('pull-right');
//                }
//            });

        </script>







        <!--load common js-->
        <script src="<?php echo Url::to("@web/mjs/common_js.js"); ?>"></script>
        <!--load common CSS-->
        <link rel="stylesheet" href="<?php echo Url::to("@web/mcss/common_css.css"); ?>" >


        <!-- select2 css js and bootstrap theme-->
        <link rel="stylesheet" href="<?php echo Url::to("@web/mcss/select2.min.css"); ?>" >
        <link rel="stylesheet" href="<?php echo Url::to("@web/mcss/select2-bootstrap.css") ?>" >
        <script src="<?php echo Url::to("@web/mjs/select2.min.js"); ?>"></script>

        <link rel="stylesheet" href="<?php echo Url::to("@web/mcss/jquery-ui.css"); ?>" >
        <script type="text/javascript" src="<?php echo Url::to("@web/mjs/jquery-ui.js"); ?>"></script>

        <script src="<?php echo Url::to("@web/mjs/jquery-ui-timepicker-addon.min.js"); ?>"></script>
        <script src="<?php echo Url::to("@web/mjs/jquery-ui-sliderAccess.js"); ?>"></script>

        <link rel="stylesheet" href="<?php echo Url::to("@web/mcss/jquery-ui-timepicker-addon.min.css"); ?>" >

        <?php $this->endBody() ?>
    </body>
</html>

<?php $this->endPage() ?>
