<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use \yii\helpers\Url;

AppAsset::register($this);
?>

<?php
$pageM = Yii::$app->controller->id . "/" . Yii::$app->controller->action->id;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (!isset($_SESSION['login_info'])) {

    if (!isset($_POST['Users']) && $pageM != "users/login") {
        echo '<script>window.location="' . Url::to(['users/login']) . '";</script>';
//            header('Location:index.php?r=users/login');
        exit;
    }
}
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <link rel="icon" href="<?php echo Url::to("@web/images/favicon.png"); ?>" type="image/gif" sizes="16x16">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>    
        <style type="text/css">
            #sticky {
                width:100%;
                padding:16px;
                padding-bottom:16px;
                background:black;
                color:white;
                font-weight:bold;
                font-size:18px;
                text-align:center;
                position:fixed;    /*Here's what sticks it*/
                bottom:0;          /*to the bottom of the body*/
                left:0;            /*and to the left of the body.*/
                z-index:999999;
                /*EFFECTS*/
                background:rgba(0, 0, 0, .3) /*the a value of .5 makes it transparent*/
            }
            .logoutBtn{
                color: #ecf0f1 !important;
            } 
            .logoutBtn:hover{
                text-decoration: none;

            } 


            .navbar-inverse  {
                background-color: #36924f;
                border-color: #217e3a;
            }
            .navbar-inverse  .navbar-brand {
                color: #ecf0f1;
            }
            .navbar-inverse  .navbar-brand:hover,
            .navbar-inverse  .navbar-brand:focus {
                color: #ffffff;
            }
            .navbar-inverse  .navbar-text {
                color: #ecf0f1;
            }
            .navbar-inverse  .navbar-nav > li > a {
                color: #ecf0f1;
            }
            .navbar-inverse  .navbar-nav > li > a:hover,
            .navbar-inverse  .navbar-nav > li > a:focus {
                color: #ffffff;
            }
            /*.navbar-inverse  .navbar-nav  .dropdown > ul > li  > a,*/
            .navbar-inverse  .navbar-nav  .dropdown  > ul > li  > a:hover{
                color: #ffffff;
                background-color: #217e3a;
            }
            .navbar-inverse  .navbar-nav > .active > a,
            .navbar-inverse  .navbar-nav > .active > a:hover,
            .navbar-inverse  .navbar-nav > .active > a:focus {
                color: #ffffff;
                background-color: #217e3a;
            }
            .navbar-inverse  .navbar-nav > .open > a,
            .navbar-inverse  .navbar-nav > .open > a:hover,
            .navbar-inverse  .navbar-nav > .open > a:focus {
                color: #ffffff;
                background-color: #217e3a;
            }
            .navbar-inverse  .navbar-toggle {
                border-color: #217e3a;
            }
            .navbar-inverse  .navbar-toggle:hover,
            .navbar-inverse  .navbar-toggle:focus {
                background-color: #217e3a;
            }
            .navbar-inverse  .navbar-toggle .icon-bar {
                background-color: #ecf0f1;
            }
            .navbar-inverse  .navbar-collapse,
            .navbar-inverse  .navbar-form {
                border-color: #ecf0f1;
            }
            .navbar-inverse  .navbar-link {
                color: #ecf0f1;
            }
            .navbar-inverse  .navbar-link:hover {
                color: #ffffff;
            }

            @media (max-width: 767px) {
                .navbar-inverse  .navbar-nav .open .dropdown-menu > li > a {
                    color: #ecf0f1;
                }
                .navbar-inverse  .navbar-nav .open .dropdown-menu > li > a:hover,
                .navbar-inverse  .navbar-nav .open .dropdown-menu > li > a:focus {
                    color: #ffffff;
                }
                .navbar-inverse  .navbar-nav .open .dropdown-menu > .active > a,
                .navbar-inverse  .navbar-nav .open .dropdown-menu > .active > a:hover,
                .navbar-inverse  .navbar-nav .open .dropdown-menu > .active > a:focus {
                    color: #ffffff;
                    background-color: #217e3a;
                }
            }
        </style>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <div class="navbar-inverse ">
                <?php
                NavBar::begin([
                    'brandLabel' => 'JP Handicraft',
//                'brandUrl' => Yii::$app->homeUrl,
                    'brandUrl' => ['/site/index'],
                    'options' => [
                        'class' => 'navbar-inverse navbar-fixed-top',
                    ],
                ]);
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'items' => [
                        ['label' => 'Home', 'url' => ['/site/index']],
                        ['label' => 'Main', 'url' => ['/employee/index']],
                        [
                            'label' => 'Master Dropdown',
                            'items' => [
                                ['label' => 'Financial Year', 'url' => ['/financial-year/index']],
                                ['label' => 'Group', 'url' => ['/group/index']],
                                ['label' => 'Area', 'url' => ['/area/index']],
                                ['label' => 'Raw Material', 'url' => ['/raw-material/index']],
                                ['label' => 'Product', 'url' => ['/product/index']],
                                ['label' => 'Product Raw Material', 'url' => ['/product-raw-material/index']],
                            ],
                        ],
                        [
                            'label' => 'User',
                            'items' => [
                                ['label' => 'Add User', 'url' => ['/users/index/']],
                                ['label' => 'User Level', 'url' => ['/user-levels/index/']],
                            ],
                        ],
                        (
                        '<li>'
                        . Html::beginForm(['/users/logout'], 'post', ['class' => 'navbar-form'])
                        . Html::hiddenInput("logBtn", "789")
                        . Html::submitButton(
                                'Logout', ['class' => 'btn btn-link logoutBtn']
                        )
                        . Html::endForm()
                        . '</li>'
                        ),
//                Yii::$app->user->isGuest ? (
//                        ['label' => 'Login', 'url' => ['/site/login']]
//                        ) : (
//                        '<li>'
//                        . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
//                        . Html::submitButton(
//                                'Logout', ['class' => 'btn btn-link']
//                        )
//                        . Html::endForm()
//                        . '</li>'
//                        )
                    ],
                ]);
                NavBar::end();

//             . Html::submitButton(
//                                    'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link']
//                            )
                ?>
            </div>
            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= $content ?>
            </div>
        </div>


        <footer class="footer">
            <div class="container">
                <!--<p class="pull-left">&copy; My Company <?= date('Y') ?></p>-->

                <!--<p class="pull-right"><?= Yii::powered() ?></p>-->
            </div>
        </footer>

        <!--application down message-->
        <!--<div id="sticky">We are updating application after 2.10PM Today.Please complete and save your work.</div>-->

        <!--load common js-->
        <script src="<?php echo Url::to("@web/mjs/common_js.js"); ?>"></script>
        <!--load common CSS-->
        <link rel="stylesheet" href="<?php echo Url::to("@web/mcss/common_css.css"); ?>" />

        <!--for //digitalbush.com/projects/masked-input-plugin/     for strict date format into text box-->
        <script src="<?php echo Url::to("@web/mjs/jquery.maskedinput.min.js"); ?>"></script>



        <?php
//        $getController = Yii::$app->controller->id;
//        $getControllerAction = Yii::$app->controller->action->id;
//        if (($getController != "family-details" && ($getControllerAction != "update" || $getControllerAction != "create")) && ($getController != "site" && ($getControllerAction != "graph-agriculture"))) {
            ?>
            <!-- select2 css js and bootstrap theme-->
            <link rel="stylesheet" href="<?php echo Url::to("@web/mcss/select2.min.css"); ?>" />
            <link rel="stylesheet" href="<?php echo Url::to("@web/mcss/select2-bootstrap.css") ?>" />
            <script id='selectTwoJs' src="<?php echo Url::to("@web/mjs/select2.min.js"); ?>"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("select").select2({
                        width: '100%',
                        theme: "bootstrap",
                        cache: true
                    });
                });
            </script>
            <?php
//        }
        ?>

        <link rel="stylesheet" href="<?php echo Url::to("@web/mcss/jquery-ui.css"); ?>" />
        <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">-->
        <!--<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->
        <script type="text/javascript" src="<?php echo Url::to("@web/mjs/jquery-ui.js"); ?>"></script>

        <script src="<?php echo Url::to("@web/mjs/jquery-ui-timepicker-addon.min.js"); ?>"></script>
        <script src="<?php echo Url::to("@web/mjs/jquery-ui-sliderAccess.js"); ?>"></script>

        <link rel="stylesheet" href="<?php echo Url::to("@web/mcss/jquery-ui-timepicker-addon.min.css"); ?>" />
        <?php $this->endBody() ?>
    </body>
</html>

<?php $this->endPage() ?>
