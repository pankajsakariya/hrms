<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProgramDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Program Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-details-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Program Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'program_name',
            'month',
            'program_code',
            'set',
            // 'number_of_product',
            // 'total',
            // 'product_id',
            // 'color',
            // 'entry_by',
            // 'created_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
