<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductRawMaterial */

$this->title = 'Create Product Raw Material';
$this->params['breadcrumbs'][] = ['label' => 'Product Raw Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-raw-material-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
