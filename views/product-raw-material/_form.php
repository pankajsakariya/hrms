<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductRawMaterial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-raw-material-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')->dropDownList(yii\helpers\ArrayHelper::map(app\models\Product::find()->all(), 'id', 'product_name'), ['class' => 'form-control', 'prompt' => 'Select']) ?>

    <?= $form->field($model, 'raw_material_id')->dropDownList(yii\helpers\ArrayHelper::map(app\models\RawMaterial::find()->all(), 'id', 'material_name'), ['class' => 'form-control', 'prompt' => 'Select']) ?>

    <?= $form->field($model, 'quantity')->textInput(['maxlength' => true, "onkeypress" => "return isNumberKey(event)"]) ?>

    <?= $form->field($model, 'weight')->textInput(['maxlength' => true, "onkeypress" => "return isNumberKey(event)"]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#productrawmaterial-weight').on('change', function () {
            if ($(this).val() !== "") {
                $(this).val(parseFloat($(this).val()).toFixed(6));
            }
        });
    });
</script>