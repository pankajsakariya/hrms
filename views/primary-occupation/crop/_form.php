<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Crop */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="crop-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'crop_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'practice_id')->dropDownList(yii\helpers\ArrayHelper::map(\app\models\AgriculturePractice::find()->all(), 'id', 'practice_name'), ['class' => 'form_control'])->label('Practice Name'); ?>

    <?= $form->field($model, 'does_it_have_by_product')->radioList(array('yes' => 'Yes', 'no' => 'No')); ?>
    <div id="productId">
        <?= $form->field($model, 'by_product_1')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'by_product_2')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('input[type="radio"]').change(function () {
            if ($(this).val() === 'yes') {
                $('#productId').find('input').prop("disabled", false);

            } else {
                $('#productId').find('input').prop("disabled", true);

            }
        });
    });
</script>
