<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CropSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Crops';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crop-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Crop', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'crop_name',
            [
                'attribute' => 'practice_id',
                'header' => 'Practice Name',
                'filter' => '',
                'content' => function($data) {
                    $modelm = app\models\AgriculturePractice::find()->where(["id" => $data->practice_id])->one();
                    if ($modelm != null) {
                        return $modelm->practice_name;
                    }
                }
                    ],
                    'does_it_have_by_product',
                    'by_product_1',
                    'by_product_2',
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>
</div>
