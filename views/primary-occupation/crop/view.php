<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Crop */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Crops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crop-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'crop_name',
            [
                'attribute' => 'practice_id',
                'header' => 'Practice Name',
                'value' => (($modelm = \app\models\AgriculturePractice::find()->where(['id' => $model->practice_id])->one()) != null) ? $modelm->practice_name : ''
            ],
            'does_it_have_by_product',
            'by_product_1',
            'by_product_2',
        ],
    ])
    ?>

</div>
