<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PrimaryOccupation */

$this->title = 'Create Primary Occupation';
$this->params['breadcrumbs'][] = ['label' => 'Primary Occupations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="primary-occupation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
