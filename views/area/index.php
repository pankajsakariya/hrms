<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Areas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="area-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Area', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'group_id',
                'format' => 'raw',
                'content' => function ($data) {
                    $model = \app\models\Group::find()->where(['id' => $data->group_id])->one();
                    $value = $model != null ? $model->group_area_name : "";
                    return $value;
                }],
                    'area_name',
                    'area_code',
                    // created_on',
                    // 'enter_by',
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>
</div>
