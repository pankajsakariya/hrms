<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RawMaterial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="raw-material-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'material_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weight')->textInput(["onkeypress" => "return isNumberKey(event)"]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#rawmaterial-weight').on('change', function () {
            if ($(this).val() !== "") {
                $(this).val(parseFloat($(this).val()).toFixed(6));
            }
        });
    });
</script>