<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserLevels */
/* @var $form yii\widgets\ActiveForm */
?>


<!--<script type="text/javascript">
        $(document).ready(function(){
            
            $("select").select2();
//            ,disabled!='disabled'
            
//            $("select[multiple!='multiple']").select2();
        })
    </script>-->

<style type="text/css">

    .bsWarningColor{
        font-weight: 600;
        padding: 5px;
        border-radius: 3px;
        /*display: list-item !important;*/
        min-height: 70px;
        line-height: 30px;
        text-align: center;
        vertical-align: central;
    }

    .bsAgriBgColor{
        background-color: #c1c215;
    }
    .bsDairyBgColor{  
        background-color: #7fffd4;
    }
    .bsHouseholdBgColor{
        background-color: #ddcbcf;
    }
    .bsDefaultBgColor{
        background-color: #f0ad4e;
    }
    .bsSimpleDesign{
        padding: 5px 10px;
        /*width: 20px;*/
        margin-right: 5px;
        display: inline-block;
        font-size: 14px;
        border-radius: 3px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="bsAgriBgColor bsSimpleDesign">Agriculture</div>
        <!--    </div>
            <div class="col-md-1">-->
        <div class="bsDairyBgColor bsSimpleDesign">Dairy</div>
        <!--    </div>
            <div class="col-md-1">-->
        <div class="bsHouseholdBgColor bsSimpleDesign">Household</div>
        <!--    </div>
            <div class="col-md-1">-->
        <div class="bsDefaultBgColor bsSimpleDesign">Other</div>
    </div>
</div>
<br>
<div class="user-levels-form">


    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'level_name')->textInput(['maxlength' => true]) ?>


    <?php
    $levelArrayFromDb = '';
    if (isset($model->level_authentications)) {
        if ($model->level_authentications != "") {
            $levelArrayFromDb = $model->level_authentications;
        }
    }
    ?>

    <?php
//        echo $form->field($model, 'level_type')
//                    ->dropDownList(
//                        getLevelTypeArray() ,          // Flat array ('id'=>'label')
//                        ['prompt'=>'Select Level...']    // options
//                    );
    ?>


    <h3>Access on this level</h3>
    <!--<br/>-->



    <?php
    $allUserList = \app\models\Users::find()->all();

    $MUSerLIst = array();
    for ($i = 0; $i < count($allUserList); $i++) {
        $singleUser = $allUserList[$i];


        $userLevel = \app\models\UserLevels::find()->where(["id" => $singleUser->user_level])->one();
        if ($userLevel != null) {
            $MUSerLIst[$singleUser->id] = $singleUser->first_name . " " . $singleUser->last_name . " {User Level : " . $userLevel->level_name . "} - {User Type: " . $singleUser->user_type . "}";
        }
    }
    ?>




    <?php
    $blockEnableDisableList = array();
//    $blockEnableDisableList[] = "centers";


    $basicActions = array();
    $basicActions[] = "create";
    $basicActions[] = "update";
    $basicActions[] = "index";
    $basicActions[] = "delete";
    $basicActions[] = "view";

    $moduleList = array();

    $moduleList[] = "financial-year";
    $moduleList[] = "primary-occupation";
    $moduleList[] = "page-authentications";
    $moduleList[] = "site";
    $moduleList[] = "units";
    $moduleList[] = "user-levels";
    $moduleList[] = "user-type";
    $moduleList[] = "users";
//display_array($moduleList);
//exit() ;






    $labeling = [];
    $labeling["financial-year"] = "Fiancial_year";
    $labeling["page-authentications"] = "Page_authentication";
    $labeling["primary-occupation"] = "Primary_occupation";
    $labeling["site"] = "Site";
    $labeling["units"] = "Units";
    $labeling["user-levels"] = "User_levels";
    $labeling["user-type"] = "User_type";
    $labeling["users"] = "User";

    $bgGroup = [];
    $bgGroup["financial-year"] = "Household";
    $bgGroup["primary-occupation"] = "Household";
    $bgGroup["page-authentications"] = "Other";
    $bgGroup["site"] = "Other";
    $bgGroup["units"] = "Other";
    $bgGroup["user-levels"] = "Other";
    $bgGroup["user-type"] = "Other";
    $bgGroup["users"] = "Other";
    ?>


    <div class="row">






        <?php
        $pagerAuthenticationModels = null;

        $authenticationColumnValues = [];
        $approveToColumnValues = [];
        if (!$model->isNewRecord) {
            $pagerAuthenticationModels = app\models\PageAuthentications::find()->where(['level_id' => $model->id])->all();

            for ($k = 0; $k < count($pagerAuthenticationModels); $k++) {
                $singelePageM = $pagerAuthenticationModels[$k];
                $authenticationColumnValues[] = $singelePageM->authentication;
                if ($singelePageM->approve_to != "" || ($singelePageM->notify_to != "[]" && $singelePageM->notify_to != "")) {
                    $approveToColumnValues[] = $singelePageM->authentication;
                }
            }
        }


        $divide = 0;
        for ($i = 0; $i < count($moduleList); $i++) {
            echo '<div class="col-lg-3">';

            $actions = $basicActions;





            if ($moduleList[$i] == 'approval') {
                $actions = array();
                $actions[] = 'approvals';
            }
            if ($moduleList[$i] == 'notifications') {
                $actions = array();
                $actions[] = 'notifications';
            }
            if ($moduleList[$i] == 'users') {
                $actions[] = 'profile';
                $actions[] = 'change-password';
            }


            if ($moduleList[$i] == 'site') {
                $actions[] = 'dashboard';
            }





            if (isset($labeling[$moduleList[$i]])) {
                $nameM = $labeling[$moduleList[$i]];
            } else {
                $nameM = ucfirst($moduleList[$i]);
            }
            $cssClass = '';
            if (isset($bgGroup[$moduleList[$i]])) {
                $name = $bgGroup[$moduleList[$i]];

                if ($name == 'Agriculture') {
                    $cssClass = 'bsAgriBgColor';
                } else if ($name == 'Dairy') {
                    $cssClass = 'bsDairyBgColor';
                } else if ($name == 'Household') {
                    $cssClass = 'bsHouseholdBgColor';
                } else {
                    $cssClass = 'bsDefaultBgColor';
                }
            }

            echo "<h4 class='bsWarningColor {$cssClass}'>$nameM module</h4>";
//            echo "<h4 class='bsWarningColor'>" . str_replace('-', '_', $nameM) ." module</h4>";
            echo '<div class="x_panel">';




            echo "<h5>Pages Authenticated</h5>";
            echo Html::listBox("PageAuthentication[{$i}][authentication]", $authenticationColumnValues, createUrls($actions, $moduleList[$i])
                    , ['class' => 'form-control', 'multiple' => true]);


            echo "<div style='display:none;'>";
            echo "<h4>Approval of</h4>";
            echo Html::listBox("PageAuthentication[{$i}][approve_of]", $approveToColumnValues, createUrls($actions, $moduleList[$i])
                    , ['class' => 'form-control', 'multiple' => true]);


            $releativeActions = [];
            for ($mi = 0; $mi < count($actions); $mi++) {
                $releativeActions[] = $moduleList[$i] . "/" . $actions[$mi];
            }
            $finalApproveTo = '';
            $finalNotifyTo = array();
            if ($pagerAuthenticationModels != null) {
                $notifytoGot = false;
                for ($k = 0; $k < count($pagerAuthenticationModels); $k++) {
                    $singelePageM = $pagerAuthenticationModels[$k];



                    if ($singelePageM->approve_to != "") {
                        if (in_array($singelePageM->authentication, $releativeActions)) {
                            $finalApproveTo = $singelePageM->approve_to;
                        }
                    }


                    if ($singelePageM->notify_to != "[]" || $singelePageM->notify_to != "") {
                        if (in_array($singelePageM->authentication, $releativeActions)) {
                            if ($notifytoGot == false) {
                                $finalNotifyTo = json_decode($singelePageM->notify_to, false);
                                $notifytoGot = true;
                            }
                        }
                    }
                }
            }

//        echo "<h4>Approval To</h4>";yii\helpers\ArrayHelper::map(\app\models\Users::find()->all(), 'id', 'user_name')
            echo Html::dropDownList("PageAuthentication[{$i}][approve_to]", $finalApproveTo, $MUSerLIst, ['prompt' => 'Select.', 'class' => 'form-control']);



            echo "<h4>Notify To</h4>";
            echo Html::listBox("PageAuthentication[{$i}][notify_to]", $finalNotifyTo, $MUSerLIst, ['multiple' => true, 'class' => 'form-control']);

            echo "</div>";
            echo '</div>';

            echo '</div>';

            $divide++;
            if ($divide % 4 == 0) {
                echo '<div class="clearfix"></div>';
            }
        }
        ?>


    </div>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
