<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Employee', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'area_id',
                'format' => 'raw',
                'content' => function ($data) {
                    $model = \app\models\Area::find()->where(['id' => $data->area_id])->one();
                    $value = $model != null ? $model->area_name : "";
                    return $value;
                }],
//            'group_id',
            'employee_name',
            'mobile',
            // 'address',
            // 'aadhar_no',
            // 'bank_account_no',
            // 'bank_name',
            // 'ifsc_code',
            // 'id_proof_photo',
            // 'employee_photo',
            // 'reference',
            // 'status',
            // 'created_on',
            // 'enter_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
