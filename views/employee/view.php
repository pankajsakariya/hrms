<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Employees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>
    <?php
    $cmodel = \app\models\Area::find()->where(['id' => $model->area_id])->one();
    $value = $cmodel != null ? $cmodel->area_name : '';
    ?> 
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'area_id',
                'value' => $value,
            ],
//            'group_id',
            'employee_name',
            'mobile',
            'address',
            'aadhar_no',
            'bank_account_no',
            'bank_name',
            'ifsc_code',
            'id_proof_photo',
            'employee_photo',
            'reference',
            'status',
//            'created_on',
//            'enter_by',
        ],
    ])
    ?>

</div>
