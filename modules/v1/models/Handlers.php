<?php

/*
 * This class contains utilities for handlers that can be used for apis
 */

namespace app\modules\v1\models;

use app\modules\v1\models\Config;
use Firebase\JWT\JWT;

class Handlers {
    /*
     * Checks api that was requested is acceptable by us or not
     * ex. user called api with GET method but we are not accepting tha api with get method than
     * this method will give false
     * @miral
     */

    public static function isValidMethod($method) {
        $request = \Yii::$app->request;

        if ($method == "post" && $request->isPost /*&& count(\Yii::$app->request->get()) == 0*/) {
            return true;
        } else if ($method == "get" && $request->isGet /*&& count(\Yii::$app->request->post()) == 0*/) {
            return true;
        } else if ($method == "put" && $request->isPut /*&& count(\Yii::$app->request->post()) == 0*/) {
            return true;
        } else if ($method == "delete" && $request->isDelete /*&& count(\Yii::$app->request->post()) == 0*/) {
            return true;
        }
        return false;
    }

    /*
     * It is used to set api as JSON
     * @miral
     */

    public static function setJsonResponse() {
        $response = \Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    }

    /*
     * Get response Instance
     * @miral
     */

    public static function getResponseInstance() {
        $response = \Yii::$app->response;
        return $response;
    }

    /*
     * Get Request Instnance
     * @miral
     */

    public static function getRequestInstance() {
        $request = \Yii::$app->request;
        return $request;
    }

    /*
     * Get params that comes with any request
     * @miral
     */

    public static function getParams($method="") {
        if($method==""){
            return json_decode(file_get_contents("php://input"), true);
        }
        else if($method=="get"){
            return $_GET;
        }
        else if($method=="post"){
            return $_POST;
        }
    }

    /*
     * Validating the input request (will throw 405)
     * A request method is not supported for the requested resource; 
     * @miral
     */

    public static function validateRequest($definedMethods, $action) {
        self::setJsonResponse();

        if ((isset($definedMethods[$action->id]) &&
                !self::isValidMethod($definedMethods[$action->id])) ||
                !isset($definedMethods[$action->id])) {
            throw new \yii\web\HttpException(405, 'Method Not Allowed');
        }
    }

    /*
     * Validating the token (will throw 401)
     * @miral
     */

    public static function validateToken($methods, $action) {
        $request = self::getRequestInstance();
        if (in_array($action->id, $methods)) {
            if ((isset($request->getHeaders()[Config::$tokenParamName]) &&
                    self::isValidToken($request->getHeaders()[Config::$tokenParamName]) == false) || !isset($request->getHeaders()[Config::$tokenParamName])) {
                throw new \yii\web\HttpException(401, 'Unauthorized');
            }
        }

//        display_array($action->id);
//        exit;
    }

    /*
     * Check is token given by user is valid
     * @miral
     */

    public static function isValidToken($token) {

        $token = JWT::decode($token, Config::$jwtKey, Config::$jwtEncodeAlgo);
        $token = self::object_to_array($token);

        if (isset($token[Config::$payloadKey])) {
            return true;
        }
        return false;
    }

    /*
     * get data in token
     * Note: In this method it is considered that you compulsarily have a token in parameter or in header
     * if you do not have token at in both the places than it will 
     * @miral
     */

    public static function extractToken($token = "") {

        if ($token == "") {
            $request = self::getRequestInstance();
            if (isset($request->getHeaders()[Config::$tokenParamName])) {
                $token = $request->getHeaders()[Config::$tokenParamName];
            }
        }

        $token = JWT::decode($token, Config::$jwtKey, Config::$jwtEncodeAlgo);
        $token = self::object_to_array($token);

        if (isset($token[Config::$payloadKey])) {
            return $token;
        }
        return false;
    }

    /*
     * will give you generated token
     * @miral
     */

    public static function generateToken($uuid, $deviceID, $login_reference) {

        $token = array(
            Config::$payloadKey => $uuid,
            Config::$payloadDeviceKey => $deviceID,
            Config::$payloadLoginReferenceKey => $login_reference
        );
        $jwt = \Firebase\JWT\JWT::encode($token, Config::$jwtKey);
        return $jwt;
    }

    /*
     * utility that convers object to array it is used in jwt that gives response in Object format
     * we need that response in array format
     * @miral
     */

    public static function object_to_array($data) {
        if (is_array($data) || is_object($data)) {
            $result = array();
            foreach ($data as $key => $value) {
                $result[$key] = self::object_to_array($value);
            }
            return $result;
        }
        return $data;
    }
    
    
    public static function generateLoadable($model,$method = ""){
        $modelName =  explode("\\",get_class($model));
        if(count($modelName)>0){
            
            $params[$modelName[count($modelName)-1]] = self::getParams($method); 
            
            return $params;
        }
        return "";
    }
    public static function getPayloadKeyValue(){
        $envelop = self::extractToken();
        return $envelop[Config::$payloadKey];
    } 
    

}
