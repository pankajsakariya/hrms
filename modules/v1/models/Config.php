<?php

namespace app\modules\v1\models;

class Config {

    public static $jwtKey = "DSSDGASBMNVDS!@#$%^&*";
    public static $tokenParamName = "token";
    public static $payloadKey = "uuid";
    public static $payloadDeviceKey = "device_id";
    public static $payloadLoginReferenceKey = "login_reference";
    public static $jwtEncodeAlgo = array('HS256');
    public static $statusCodeSuccess = "200";
    public static $statusCodeUnAuthorized = "401";
    public static $statusCodeLocked = "423";
    public static $statusCodeNoContent = "204";
    public static $messageLoginSuccess = "Login successfully";
    public static $messageDataSaveSuccess = "Saved successfully";
    public static $messageDataGetSuccess = "Data fetched successfully";

    public function getResponseCodes() {

        return [$statusCodeSuccess => 200];
    }

}
