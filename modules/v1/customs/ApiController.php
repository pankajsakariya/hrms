<?php

namespace app\modules\v1\customs;

use yii\web\Controller;
use app\modules\v1\models\Handlers;

abstract class ApiController extends Controller {
    //put your code here
    
    /*
     * it will remove default yii token validation
     * because we need another token validation mechanism
     * @miral
     */
    public $enableCsrfValidation = false;
    
    /*
     * Define which action will accept which method
     * @miral
     */
    abstract public function defineMethods();
    
    /*
     * Define which action needs token validation
     * For Ex. login does not need token validation so dont inclued it hear
     * @miral
     */
    abstract public function checkTokenForMethods();
    
    
    
    /*
     * That method will be called befor any action
     * means any api needs to go threw this method befor any business logic
     * it mainly contains validation of request and validation of token
     * @miral
     */
    public function beforeAction($action) {
        parent::beforeAction($action);
                
        Handlers::setJsonResponse();
        Handlers::validateRequest($this->defineMethods(), $action);
        Handlers::validateToken($this->checkTokenForMethods(), $action);

        return true;
    }

    
    
    protected function findModelByQuery($query) {
        $model = $query->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Not Found');
        }
    }
    
}
