<?php

namespace app\modules\v1\controllers;

use app\modules\v1\models\Config;
use app\modules\v1\models\Handlers;
use app\modules\v1\customs\ApiController;

class CattleController extends ApiController {

    public function defineMethods() {
        return [
            "list" => "get",
            "update" => "put",
        ];
    }

    public function checkTokenForMethods() {
        return [
            "list",
            "update",
        ];
    }

    public function actionList() {

        $response = Handlers::getResponseInstance();
        $token = Handlers::extractToken();
        // cattle list household wise
        $cattleData = \app\models\CattleProfile::find()->asArray()
                ->select(['cattle_profile.id', 'cattle_profile.cattle_id', 'cattle_profile.cattle_name', 'cattle_profile.cattle_type', 'cattle_profile.breed', 'cattle_profile.born_date', 'cattle_profile.mother_id', 'cattle_profile.purchased_date', 'cattle_profile.purchase_amount', 'cattle_profile.sale_date', 'cattle_profile.death_date', 'cattle_profile.cattle_entry_type', 'cattle_profile.reason', 'cattle_profile.`status`', 'cattle_profile.cattle_sale_or_death_date', 'cattle_profile.cattle_sale_or_death_amount', 'milking_status', 'cattle_profile.heat_cycle_date',
                    '(SELECT count(ai.id) from artificial_insemination_details as ai WHERE ai.cattle_profile_id = cattle_profile.id) as  `totalAiCount`',
                    '(SELECT count(ai.id) from artificial_insemination_details as ai WHERE ai.cattle_profile_id = cattle_profile.id && ai.ai_status = "Success") as  `totalAiSuccessCount`',
                    '(SELECT count(ai.id) from artificial_insemination_details as ai WHERE ai.cattle_profile_id = cattle_profile.id && ai.ai_status = "Failure") as  `totalAiFailureCount`',
                    '(SELECT count(ai.id) from artificial_insemination_details as ai WHERE ai.cattle_profile_id = cattle_profile.id and ai.delivery_status = "Success") as  `totalAiSuccessDeliveryCount`'])
                ->where(['cattle_profile.household_id' => $token[Config::$payloadKey]])
                ->all();

        if ($cattleData == NULL) {
            throw new \yii\web\HttpException(404, 'No Data Available');
        }

        $response->data = $cattleData;
        return $response;
    }

    public function actionUpdate($id) {

        $model = $this->findModelByQuery(\app\models\CattleProfile::find()->where([
                    'household_id' => Handlers::getPayloadKeyValue(),
                    "id" => $id]));
        if ($model->load(Handlers::generateLoadable($model)) && $model->save()) {
            //successfully update
//            return $model->attributes;
        } else {
            return $model->errors;
        }
    }

}
