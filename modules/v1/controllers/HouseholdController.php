<?php

namespace app\modules\v1\controllers;

use app\modules\v1\models\Config;
use app\modules\v1\models\Handlers;
use app\modules\v1\customs\ApiController;

class HouseholdController extends ApiController {

    public function defineMethods() {
        return [
            "login" => "post",
            "profile-data" => "get",
            "view-token" => "get",
            "password-recovery" => "post",
        ];
    }

    public function checkTokenForMethods() {
        return [
            "profile-data",
            "view-token"
        ];
    }

    public function actionLogin() {


        $response = Handlers::getResponseInstance();

        $requstedData = Handlers::getParams();

        // requierd field not set
        if (!isset($requstedData["username"], $requstedData["password"], $requstedData["device_id"])) {
            throw new \yii\web\HttpException(404, 'Bad Request');
        }

        $username = $requstedData["username"];
        $password = $requstedData["password"];
        $device_id = $requstedData["device_id"];
        $ip_address = isset($requstedData["ip_address"]) ? $requstedData["ip_address"] : "";
        $os_version = isset($requstedData["os_version"]) ? $requstedData["os_version"] : "";
        $manufacture = isset($requstedData["manufacture"]) ? $requstedData["manufacture"] : "";
        $mac_address = isset($requstedData["mac_address"]) ? $requstedData["mac_address"] : "";
        $mobile_number = isset($requstedData["mobile_number"]) ? $requstedData["mobile_number"] : "";
        $device_model = isset($requstedData["device_model"]) ? $requstedData["device_model"] : "";


        //check username password is valid ?
        $credentialCheck = \app\models\HouseholdProfile::find()->asArray()->where([
                    'username' => $username,
                    'password' => $password
                ])->one();

        if ($credentialCheck == NULL) {
            throw new \yii\web\HttpException(401, 'Unauthorized');
        }



        //check household ID and device ID is blocked?
        $blockStatus = \app\models\BlockHistory::find()->asArray()->select('blocked_status')->where([
                    'household_id' => $credentialCheck['id'],
                    'device_id' => $device_id
                ])->one();

        if ($blockStatus != NULL && $blockStatus['blocked_status'] == 1) {
            throw new \yii\web\HttpException(423, 'Account Locked');
        }

        //check login history
        $loginHistory = \app\models\HouseholdLoginHistory::find()->where([
                    'household_id' => $credentialCheck['id'],
                    'device_id' => $device_id,
                    'logout_status' => 0
                ])->all();

        foreach ($loginHistory as $value) {
            $value->logout_status = 1;
            $value->logout_time = date('Y-m-d H:i:s');
            $value->cleared_logout = 1;
            $value->save(false);
        }


        //save login history
        $insertLoginHistory = new \app\models\HouseholdLoginHistory();
        $insertLoginHistory->household_id = $credentialCheck['id'];
        $insertLoginHistory->device_id = $device_id;
        $insertLoginHistory->ip_address = $ip_address;
        $insertLoginHistory->login_time = date('Y-m-d H:i:s');
        $insertLoginHistory->save(false);


        $token = Handlers::generateToken($credentialCheck['id'], $device_id, $insertLoginHistory->id);

        $successCode = $response->statusCode = 200;
        $response->data = [
            'token' => $token,
            'message' => Config::$messageLoginSuccess,
            "status" => $successCode];
        return $response;
    }

    //only for view token details
    public function actionViewToken() {
        $response = Handlers::getResponseInstance();
        $token = Handlers::extractToken();
        $successCode = $response->statusCode = 200;
        $response->data = [
            'token' => $token,
            'message' => Config::$messageDataGetSuccess,
            "status" => $successCode];
        return $response;
    }

    //itself household data
    public function actionProfileData() {

        $response = Handlers::getResponseInstance();
        $token = Handlers::extractToken();
        $houseHoldData = \app\models\HouseholdProfile::find()->asArray()
                        ->select(['household_profile.`name`', 'household_profile.mobile', 'household_profile.village as `village_id`', 'village.village', 'household_profile.type_of_farmer', 'household_profile.financial_year', 'household_profile.household_code', 'household_profile.`status`'])
                        ->where(['household_profile.id' => $token[Config::$payloadKey]])
                        ->innerJoin('village', 'household_profile.village = village.id')->one();

        if ($houseHoldData == NULL) {
            throw new \yii\web\HttpException(404, 'No Data Available');
        }

//        $successCode = $response->statusCode = 200;
        $response->data = [
            'data' => $houseHoldData,
            'message' => Config::$messageDataGetSuccess];
        return $response;
    }

    public function actionPasswordRecovery() {

        $response = Handlers::getResponseInstance();
//        $token = Handlers::extractToken();
        $requstedData = Handlers::getParams();


        // requierd field not set
        if (!isset($requstedData["username"])) {
            throw new \yii\web\HttpException(404, 'Bad Request');
        }

        $username = $requstedData["username"];

        $houseHoldData = \app\models\HouseholdProfile::find()->asArray()
                ->select(['household_profile.username',
                    'household_profile.`name`',
                    'household_profile.`password`'])
                ->where(['household_profile.username' => $username])
                ->one();
        $str = "{$houseHoldData['name']}, your password for UserID : {$houseHoldData['username']} is {$houseHoldData['password']}";
        if ($houseHoldData == NULL) {
            throw new \yii\web\HttpException(404, 'No Data Available');
        }

//        $successCode = $response->statusCode = 200;
        $response->data = [
            'data' => $str,
            'message' => Config::$messageDataGetSuccess];
        return $response;
    }

}
