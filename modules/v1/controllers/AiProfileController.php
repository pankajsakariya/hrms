<?php

namespace app\modules\v1\controllers;

use app\modules\v1\models\Config;
use app\modules\v1\models\Handlers;
use app\modules\v1\customs\ApiController;

class AiProfileController extends ApiController {

    public function defineMethods() {
        return [
            "list" => "get"
        ];
    }

    public function checkTokenForMethods() {
        return [
            "list"
        ];
    }

    public function actionList() {

        $response = Handlers::getResponseInstance();
        $token = Handlers::extractToken();
        $Data = \app\models\AiProfile::find()->asArray()
                ->select(['id', 'name', 'dob', 'mobile_number', 'education_qualification', 'experience_in_years', 'number_of_ais_done', 'success_rate'])
                ->all();

         
        //if not data
        if (count($Data) == 0) {
            throw new \yii\web\HttpException(404, 'No Data Available');
        }
        $response->data = $Data;
        return $response;
    }

}
