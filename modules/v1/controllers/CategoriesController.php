<?php

namespace app\modules\v1\controllers;

use app\modules\v1\models\Config;
use app\modules\v1\models\Handlers;
use app\modules\v1\customs\ApiController;

class CategoriesController extends ApiController {

    public function defineMethods() {
        return [
            "create" => "post",
            "update" => "put",
            "view" => "get",
            "index" => "get",
            "delete" => "delete"
        ];
    }

    public function checkTokenForMethods() {
        return [
            "create",
            "update",
            "view",
            "delete",
            "index"
        ];
    }

    public function actionCreate() {
        $model = new \app\models\Categories();
        if ($model->load(Handlers::generateLoadable($model)) && $model->save()) {
            return $model->attributes;
        } else {
            return $model->errors;
        }
    }
    
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->load(Handlers::generateLoadable($model)) && $model->save()) {
            return $model->attributes;
        } else {
            return $model->errors;
        }
    }
    
    public function actionView($id) {
        $model = $this->findModel($id);
        return $model->attributes;
    }
    
    public function actionIndex() {
        $searchModel = new \app\models\CategoriesSearch();
        $dataProvider = $searchModel->search(Handlers::generateLoadable($searchModel,"get"));
        return $dataProvider->models;
    }

    
    protected function findModel($id) {
        if (($model = \app\models\Categories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Not Found');
        }
    }
    
    public function actionDelete($id) {
        $this->findModel($id)->delete();
    }
}
