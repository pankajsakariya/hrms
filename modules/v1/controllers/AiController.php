<?php

namespace app\modules\v1\controllers;

use app\modules\v1\models\Config;
use app\modules\v1\models\Handlers;
use app\modules\v1\customs\ApiController;

class AiController extends ApiController {

    public function defineMethods() {
        return [
            "list" => "get"
        ];
    }

    public function checkTokenForMethods() {
        return [
            "list"
        ];
    }

    public function actionList() {

        $response = Handlers::getResponseInstance();
        $token = Handlers::extractToken();
        // cattle list household wise
        $cattleData = \app\models\CattleProfile::find()->asArray()
                ->select(['cattle_profile.id'])
                ->where(['cattle_profile.household_id' => $token[Config::$payloadKey]])
                ->all();

        if ($cattleData == NULL) {
            throw new \yii\web\HttpException(404, 'No Data Available');
        }

        $resData = array();
        if (count($cattleData) > 0) {
            for ($i = 0; $i < count($cattleData); $i++) {
                //each cattle take last 2 record one is current AI and second is history
                $aiData = \app\models\ArtificialInseminationDetails::find()
                        ->asArray()
                        ->select(['artificial_insemination_details.id', 'artificial_insemination_details.cattle_profile_id', 'artificial_insemination_details.cattle_id', 'artificial_insemination_details.insemination_type', 'artificial_insemination_details.date_of_ai_person_visit', 'artificial_insemination_details.time_of_ai_person_visit', 'artificial_insemination_details.ai_status', 'artificial_insemination_details.date_of_heatcycle_intimation', 'artificial_insemination_details.time_of_heatcycle_intimation', 'artificial_insemination_details.ai_service_provider_name', 'artificial_insemination_details.semen_details', 'artificial_insemination_details.no_remarks', 'artificial_insemination_details.pregnancy_status', 'artificial_insemination_details.pregnancy_date', 'artificial_insemination_details.pregnancy_failure_remarks', 'artificial_insemination_details.delivery_status', 'artificial_insemination_details.delivery_date', 'artificial_insemination_details.delivery_failure_remarks', 'artificial_insemination_details.new_born_cattle_id', 'artificial_insemination_details.semen_code', 'artificial_insemination_details.bull_details', 'artificial_insemination_details.type_of_bull', 'artificial_insemination_details.household_id'])
                        ->where(['artificial_insemination_details.cattle_profile_id' => $cattleData[$i]['id']])
                        ->orderBy('artificial_insemination_details.id DESC')
                        ->limit(2)
                        ->all();

                if (isset($aiData) && $aiData != NULL && count($aiData) > 0) {
                    for ($j = 0; $j < count($aiData); $j++) {
                        if ($j == 0) {
                            //jist j == 0 means its last record
                            $aiData[$j]["current"] = "true";
                            $resData[] = $aiData[$j];
                        } else {
                            //second last record
                            $aiData[$j]["current"] = "false";
                            $resData[] = $aiData[$j];
                        }
                    }
                }
            }
        }
        //if not data
        if (count($resData) == 0) {
            throw new \yii\web\HttpException(404, 'No Data Available');
        }
        $response->data = $resData;
        return $response;
    }

}
