<?php

namespace app\modules\v1\controllers;

use app\modules\v1\models\Config;
use app\modules\v1\models\Handlers;
use app\modules\v1\customs\ApiController;

class GraphController extends ApiController {

    public function defineMethods() {
        return [
            "graph-expense-income" => "get",
            "graph-expense" => "get",
            "graph-milk-production" => "get",
        ];
    }

    public function checkTokenForMethods() {
        return [
            "graph-expense-income",
            "graph-expense",
            "graph-milk-production",
        ];
    }

    public function actionGraphExpenseIncome() {

        $response = Handlers::getResponseInstance();
        $token = Handlers::extractToken();
        $data = \app\models\DairyExpense::find()->asArray()
                ->select(['SUBSTRING_INDEX(dairy_expense.month_year,"-",1) as "month",
SUBSTRING_INDEX(dairy_expense.month_year,"-",-1) as "year",
                    IFNULL(dairy_expense.feed_expense,0) + 
IFNULL(dairy_expense.green_fodder_expense,0) + 
IFNULL(dairy_expense.dry_matter_expense,0) + 
IFNULL(dairy_expense.mineral_mixture_expense,0) + 
IFNULL(dairy_expense.medicine_homebased_expense,0) + 
IFNULL(dairy_expense.medicine_allopathy_expense,0) + 
IFNULL(dairy_expense.medicine_homeopathy_expense,0) + 
IFNULL(dairy_expense.cattle_purchase_expense,0) + 
IFNULL(dairy_expense.cattle_death_expense,0) as `DairyExpense`,
IFNULL((SELECT SUM(di.income_of_milk) from dairy_income as di WHERE di.month_year = dairy_expense.month_year AND di.household_id = dairy_expense.household_id),0) as `income`'])
                ->where(['dairy_expense.household_id' => $token[Config::$payloadKey]])
                ->all();

        if ($data == NULL) {
            throw new \yii\web\HttpException(404, 'No Data Available');
        }

        $response->data = $data;
        return $response;
    }

    public function actionGraphExpense() {

        $response = Handlers::getResponseInstance();
        $token = Handlers::extractToken();
        $data = \app\models\DairyExpense::find()->asArray()
                ->select(['SUBSTRING_INDEX(dairy_expense.month_year,"-",1) as "month",
SUBSTRING_INDEX(dairy_expense.month_year,"-",-1) as "year",
IFNULL(dairy_expense.feed_expense,0) as `feed_expense`,
IFNULL(dairy_expense.green_fodder_expense,0) as `green_fodder_expense`,
IFNULL(dairy_expense.dry_matter_expense,0) as `dry_matter_expense`,
IFNULL(dairy_expense.mineral_mixture_expense,0) as `mineral_mixture_expense`,
IFNULL(dairy_expense.medicine_homebased_expense,0) as `medicine_homebased_expense`,
IFNULL(dairy_expense.medicine_allopathy_expense,0) as `medicine_allopathy_expense`,
IFNULL(dairy_expense.medicine_homeopathy_expense,0) as `medicine_homeopathy_expense`,
IFNULL(dairy_expense.cattle_purchase_expense,0) as `cattle_purchase_expense`'])
                ->where(['dairy_expense.household_id' => $token[Config::$payloadKey]])
                ->all();

        if ($data == NULL) {
            throw new \yii\web\HttpException(404, 'No Data Available');
        }

        $response->data = $data;
        return $response;
    }

    public function actionGraphMilkProduction() {

        $response = Handlers::getResponseInstance();
        $token = Handlers::extractToken();
        $data = \app\models\MilkProduction::find()->asArray()
                ->select(['SUBSTRING_INDEX(milk_production.month_year,"-",1) as "month",
                    SUBSTRING_INDEX(milk_production.month_year,"-",-1) as "year",
                    milk_production.cattle_profile_id,
                    cattle_profile.cattle_name,
                    milk_production.total,
                    milk_production.household_id'])
                ->innerJoin('cattle_profile', 'milk_production.cattle_profile_id = cattle_profile.id')
                ->where(['milk_production.household_id' => $token[Config::$payloadKey]])
                ->all();

        if ($data == NULL) {
            throw new \yii\web\HttpException(404, 'No Data Available');
        }

        $response->data = $data;
        return $response;
    }

}
