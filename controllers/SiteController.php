<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller {

    public function behaviors() {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout', 'index'],
//                'rules' => [
//                    [
//                        'actions' => ['logout', 'index'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        checkAuthentication($this);
//        return $this->redirect(['household-profile/index']);
//        return $this->redirect(['household-profile/create']);
        return $this->render('index');
    }

    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->renderPartial('login', [
                    'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    public function actionAbout() {
        return $this->render('about');
    }

    public function actionGraphAgriculture() {
//        checkAuthentication($this);
        return $this->render('graph_agriculture');
    }

    public function actionGraphDairy() {
//        checkAuthentication($this);
        return $this->render('graph_dairy');
    }

    public function actionDashboard() {
//         checkAuthentication($this);
        return $this->render('dashboard');
    }

    public function actionSample() {
        $request = Yii::$app->request;
        $response = Yii::$app->response;
        if ($request->isPost) {
            $data = $request->post();
//            display_array($data);   exit;
//            $searchname = explode(":", $data['searchname']);
            $response->format = \yii\web\Response::FORMAT_JSON;
            $successCode = $response->statusCode = 200;
            $response->data = ['searchName' => $data['searchname'], 'message' => 'Success world', "status" => $successCode];
            return $response;
        } else {
            throw new \yii\web\HttpException(400, 'Invalid request.');
        }
    }

    public function actionGetHouseholdPerVillage() {
        if ($id = Yii::$app->request->post('id')) {
            $operationPosts = \app\models\HouseholdProfile::find()->where(['village' => $id, 'status' => 'Active'])->count();
            if ($operationPosts > 0) {
                $operations = \app\models\HouseholdProfile::find()->where(['village' => $id, 'status' => 'Active'])->all();
                echo "<option value=''>Select Farmer</option>";
                foreach ($operations as $operation) {
                    echo "<option value='" . $operation->id . "'>" . $operation->name . "</option>";
                }
            } else {
                echo "<option value=''>NO DATA</option>";
            }
        } else {
            echo "<option value=''>Select Farmer</option>";
        }
    }

}
