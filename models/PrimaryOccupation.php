<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "primary_occupation".
 *
 * @property integer $id
 * @property string $primary_occupation
 * @property string $create_on
 */
class PrimaryOccupation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'primary_occupation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_on'], 'safe'],
            [['primary_occupation'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'primary_occupation' => 'Primary Occupation',
            'create_on' => 'Create On',
        ];
    }
}
