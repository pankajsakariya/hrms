<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $product_name
 * @property double $rate_wholesale
 * @property double $rate_retail
 * @property string $color
 * @property double $product_actual_weight
 * @property double $product_calculated_weight
 * @property double $product_job_charge
 * @property string $entry_by
 * @property string $created_on
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rate_wholesale', 'rate_retail', 'product_actual_weight', 'product_calculated_weight', 'product_job_charge'], 'number'],
//            [['color'], 'string'],
            [['product_name','rate_wholesale', 'rate_retail','product_job_charge'], 'required'],
            [['created_on','color'], 'safe'],
            [['product_name', 'entry_by'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_name' => 'Product Name',
            'rate_wholesale' => 'Rate Wholesale',
            'rate_retail' => 'Rate Retail',
            'color' => 'Color',
            'product_actual_weight' => 'Product Actual Weight',
            'product_calculated_weight' => 'Product Calculated Weight',
            'product_job_charge' => 'Product Job Charge',
            'entry_by' => 'Entry By',
            'created_on' => 'Created On',
        ];
    }
}
