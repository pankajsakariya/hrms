<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "units".
 *
 * @property integer $id
 * @property string $unit_name
 * @property string $identity
 * @property string $convert_rate_per_kg_and_lt
 * @property string $create_on
 */
class Units extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'units';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unit_name'], 'required'],
            [['create_on'], 'safe'],
            [['unit_name', 'convert_rate_per_kg_and_lt'], 'string', 'max' => 20],
            [['identity'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unit_name' => 'Unit Name',
            'identity' => 'Identity',
            'convert_rate_per_kg_and_lt' => 'Convert Rate Per Kg And Lt',
            'create_on' => 'Create On',
        ];
    }
}
