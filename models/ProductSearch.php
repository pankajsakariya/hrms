<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['product_name', 'color', 'entry_by', 'created_on'], 'safe'],
            [['rate_wholesale', 'rate_retail', 'product_actual_weight', 'product_calculated_weight', 'product_job_charge'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'rate_wholesale' => $this->rate_wholesale,
            'rate_retail' => $this->rate_retail,
            'product_actual_weight' => $this->product_actual_weight,
            'product_calculated_weight' => $this->product_calculated_weight,
            'product_job_charge' => $this->product_job_charge,
            'created_on' => $this->created_on,
        ]);

        $query->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'entry_by', $this->entry_by]);

        return $dataProvider;
    }
}
