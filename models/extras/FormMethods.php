<?php

namespace app\models\extras;

class FormMethods
{
    public function submitWithDefaults($model)
    {
        $model->timestamp = $this->getCurrentTimeStamp();
        $model->upload_status = "1";
        return $model->save();
    }
    public function getCurrentTimeStamp() {
        $milliseconds = round(microtime(true) * 1000);
        return $milliseconds;
    }
}
