<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "raw_material".
 *
 * @property integer $id
 * @property string $material_name
 * @property double $weight
 * @property string $entry_by
 * @property string $created_on
 */
class RawMaterial extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'raw_material';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['weight'], 'number'],
            [['created_on'], 'safe'],
            [['material_name','weight'], 'required'],
            [['material_name', 'entry_by'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'material_name' => 'Material Name',
            'weight' => 'Weight(in Kg)',
            'entry_by' => 'Entry By',
            'created_on' => 'Created On',
        ];
    }

}
