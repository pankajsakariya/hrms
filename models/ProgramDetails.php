<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "program_details".
 *
 * @property integer $id
 * @property string $program_name
 * @property string $month
 * @property string $program_code
 * @property integer $set
 * @property integer $number_of_product
 * @property integer $total
 * @property integer $product_id
 * @property string $color
 * @property string $entry_by
 * @property string $created_on
 */
class ProgramDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'program_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['set', 'number_of_product', 'total', 'product_id'], 'integer'],
            [['created_on'], 'safe'],
            [['program_name', 'month', 'program_code', 'entry_by'], 'string', 'max' => 30],
            [['color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'program_name' => 'Program Name',
            'month' => 'Month',
            'program_code' => 'Program Code',
            'set' => 'Set',
            'number_of_product' => 'Number Of Product',
            'total' => 'Total',
            'product_id' => 'Product ID',
            'color' => 'Color',
            'entry_by' => 'Entry By',
            'created_on' => 'Created On',
        ];
    }
}
