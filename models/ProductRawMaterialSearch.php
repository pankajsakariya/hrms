<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductRawMaterial;

/**
 * ProductRawMaterialSearch represents the model behind the search form about `app\models\ProductRawMaterial`.
 */
class ProductRawMaterialSearch extends ProductRawMaterial {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'product_id', 'raw_material_id'], 'integer'],
            [['weight'], 'number'],
            [['quantity', 'entry_by', 'created_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ProductRawMaterial::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            'raw_material_id' => $this->raw_material_id,
            'quantity' => $this->quantity,
            'weight' => $this->weight,
            'created_on' => $this->created_on,
        ]);

        $query->andFilterWhere(['like', 'entry_by', $this->entry_by]);

        return $dataProvider;
    }

}
