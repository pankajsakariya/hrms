<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_raw_material".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $raw_material_id
 * @property string $quantity
 * @property string $weight
 * @property string $entry_by
 * @property string $created_on
 */
class ProductRawMaterial extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_raw_material';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['product_id', 'raw_material_id'], 'integer'],
            [['weight'], 'number'],
            [['quantity', 'product_id', 'raw_material_id', 'weight'], 'required'],
            [['created_on', 'quantity'], 'safe'],
            [['entry_by'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'product_id' => 'Product',
            'raw_material_id' => 'Raw Material',
            'quantity' => 'Quantity',
            'weight' => 'Weight',
            'entry_by' => 'Entry By',
            'created_on' => 'Created On',
        ];
    }

}
