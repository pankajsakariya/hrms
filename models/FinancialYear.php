<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "financial_year".
 *
 * @property integer $id
 * @property string $financial_year
 * @property string $year
 * @property string $is_deleted
 * @property string $created_on
 */
class FinancialYear extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'financial_year';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_on'], 'safe'],
            [['financial_year'], 'string', 'max' => 20],
            [['year'], 'string', 'max' => 4],
            [['is_deleted'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'financial_year' => 'Financial Year',
            'year' => 'Year',
            'is_deleted' => 'Is Deleted',
            'created_on' => 'Created On',
        ];
    }
}
