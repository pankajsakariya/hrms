<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RingAllocation;

/**
 * RingAllocationSearch represents the model behind the search form about `app\models\RingAllocation`.
 */
class RingAllocationSearch extends RingAllocation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'program_detail_id', 'employee_id'], 'integer'],
            [['ring_type', 'ring_color', 'no_of_ring', 'ring_weight', 'on_date', 'entry_by', 'created_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RingAllocation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'program_detail_id' => $this->program_detail_id,
            'employee_id' => $this->employee_id,
            'on_date' => $this->on_date,
            'created_on' => $this->created_on,
        ]);

        $query->andFilterWhere(['like', 'ring_type', $this->ring_type])
            ->andFilterWhere(['like', 'ring_color', $this->ring_color])
            ->andFilterWhere(['like', 'no_of_ring', $this->no_of_ring])
            ->andFilterWhere(['like', 'ring_weight', $this->ring_weight])
            ->andFilterWhere(['like', 'entry_by', $this->entry_by]);

        return $dataProvider;
    }
}
