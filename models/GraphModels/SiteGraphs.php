<?php

namespace app\models\GraphModels;

class SiteGraphs {

    // *******************************
    public function getAgriculturePracticeDitribution($FinancialYear, $houseHoldId, $villageId,$crop_type) {
        $query = new \yii\db\Query;
//        $query->select([
//                    'agriculture_practice_crop_details.agriculture_practice_id',
//                    'agriculture_practice_crop_details.crop_id',
//                    'agriculture_practice.practice_name as practice_name',
//                    'SUM(agriculture_practice_crop_details.land_acreage) as land_acreage_sum',
//                    'crop.crop_name'
//                ])
//                ->from('agriculture_practice_crop_details,agriculture_practice,crop')
//                ->where('agriculture_practice_crop_details.financial_year = ' . $FinancialYear)
//                ->andWhere('agriculture_practice.id = agriculture_practice_crop_details.agriculture_practice_id')
//                ->andWhere('crop.id = agriculture_practice_crop_details.crop_id')
//                ->groupBy('agriculture_practice_crop_details.agriculture_practice_id');
//         
//         --------------------------------
        $query->select(["agriculture_practice_crop_details.agriculture_practice_id,
                            Sum(agriculture_practice_crop_details.land_acreage) AS land_acreage_sum,
                            agriculture_practice.practice_name,
                            household_profile.village as `village_id`,
                            village.village"])
                ->from('agriculture_practice_crop_details')
                ->innerJoin('agriculture_practice', 'agriculture_practice_crop_details.agriculture_practice_id = agriculture_practice.id')
                ->innerJoin('household_profile', 'agriculture_practice_crop_details.household_id = household_profile.id')
                ->innerJoin('village', 'household_profile.village= village.id')
                ->where('agriculture_practice_crop_details.financial_year =  ' . $FinancialYear);
        if (isset($houseHoldId) && $houseHoldId != "" && $houseHoldId != "ALL") {
            $query = $query->andWhere(["=", "household_profile.id", $houseHoldId]);
        }
        if (isset($villageId) && $villageId != "" && $villageId != "ALL") {
            $query = $query->andWhere(["=", "household_profile.village", $villageId]);
        }
         if (isset($crop_type) && $crop_type != "" && $crop_type != "ALL") {
            $query->andWhere(['agriculture_practice_crop_details.crop_type' => $crop_type]);
        }
//                            ->andWhere('household_profile.village = 376')
        //                ->andWhere('household_profile.id = 1')
        $query->groupBy('agriculture_practice_crop_details.agriculture_practice_id ');

        $command = $query->createCommand();
        $Output = $command->queryAll();
        return $Output;
    }

    /**/

    public function getAgriculturePracticeDitributionVillage($FinancialYear, $villageId) {
        $query = new \yii\db\Query;
        $query->select(["agriculture_practice_crop_details.agriculture_practice_id,
                            Sum(agriculture_practice_crop_details.land_acreage) AS land_acreage_sum,
                            agriculture_practice.practice_name,
                            household_profile.village as `village_id`,
                            village.village"])
                ->from('agriculture_practice_crop_details')
                ->innerJoin('agriculture_practice', 'agriculture_practice_crop_details.agriculture_practice_id = agriculture_practice.id')
                ->innerJoin('household_profile', 'agriculture_practice_crop_details.household_id = household_profile.id')
                ->innerJoin('village', 'household_profile.village= village.id')
                ->where('agriculture_practice_crop_details.financial_year =  ' . $FinancialYear);
        if (isset($villageId) && $villageId != "" && $villageId != "ALL") {
            $query = $query->andWhere(["=", "household_profile.village", $villageId]);
        }
//                            ->andWhere('household_profile.village = 376')
        //                ->andWhere('household_profile.id = 1')
        $query->groupBy('agriculture_practice_crop_details.agriculture_practice_id ');
        $command = $query->createCommand();
        $Output = $command->queryAll();
        return $Output;
    }

    // Householdwise
    public function getAgriculturePracticeDitributionHousehold($FinancialYear, $villageId) {
        $query = new \yii\db\Query;
        $query->select(["agriculture_practice_crop_details.household_id as hh_id,household_profile.name as hh_name,
                         agriculture_practice.practice_name,
                         agriculture_practice_crop_details.agriculture_practice_id,
                         COUNT(agriculture_practice_crop_details.agriculture_practice_id) as NO_Of_Practice,
                         SUM(agriculture_practice_crop_details.land_acreage) as land_acreage_sum"])
                ->from('agriculture_practice_crop_details')
                ->innerJoin('household_profile', 'agriculture_practice_crop_details.household_id = household_profile.id')
                ->innerJoin('agriculture_practice', 'agriculture_practice_crop_details.agriculture_practice_id = agriculture_practice.id')
                ->where('agriculture_practice_crop_details.financial_year =  ' . $FinancialYear);
        // ->andWhere('household_profile.village = 374');
        if (isset($villageId) && $villageId != "" && $villageId != "ALL") {
            $query = $query->andWhere(["=", "household_profile.village", $villageId]);
        }
//                            ->andWhere('household_profile.village = 376')
        //                ->andWhere('household_profile.id = 1')
        $query = $query->groupBy('agriculture_practice_crop_details.agriculture_practice_id, agriculture_practice_crop_details.household_id');
        $query = $query->orderBy('agriculture_practice_crop_details.household_id ASC');
        $command = $query->createCommand();
        $Output = $command->queryAll();
        //  display_array($Output);exit;
        return $Output;
    }

    /**/

    // Profitablity Analysis
    public function getPerAcreByFinancialYearAndCropInput($crop, $FinancialYear) {
        $query = new \yii\db\Query;
        $query->select(["
            SUM(agri_input.total_grand_agri_input) as total_grand_agri_input,
SUM(agriculture_practice_crop_details.land_acreage) as land_acreage,
ROUND(SUM(agri_input.total_grand_agri_input)/SUM(agriculture_practice_crop_details.land_acreage),2) as perAcre,
agriculture_practice_crop_details.crop_type"])
                ->from('agri_input')
                ->innerJoin('agriculture_practice_crop_details', 'agriculture_practice_crop_details.id = agri_input.agriculture_practice_crop_details_id')
                ->innerJoin('household_profile', 'agriculture_practice_crop_details.household_id = household_profile.id')
                ->where('agriculture_practice_crop_details.financial_year = ' . $FinancialYear);
        if (isset($crop) && $crop != "" && $crop != "ALL") {
            $query = $query->andWhere(["=", "agriculture_practice_crop_details.crop_id", $crop]);
        }
        $query->groupBy('agriculture_practice_crop_details.crop_type');
        $command = $query->createCommand();
        $Output = $command->queryAll();
        return $Output;
    }

    // Output
    public function getPerAcreByFinancialYearAndCropOutput($crop, $FinancialYear) {
        $query = new \yii\db\Query;
        $query->select(["ROUND(SUM(agriculture_practice_crop_details.land_acreage),2) as land_acreage,
ROUND(SUM(agri_output.production_income)/SUM(agriculture_practice_crop_details.land_acreage),2)as perAcre,
agriculture_practice_crop_details.crop_type"])
                ->from('agri_output')
                ->innerJoin('agriculture_practice_crop_details', 'agriculture_practice_crop_details.id = agri_output.agriculture_practice_crop_details_id')
                ->innerJoin('household_profile', 'agriculture_practice_crop_details.household_id = household_profile.id')
                ->where('agriculture_practice_crop_details.financial_year = ' . $FinancialYear);
        if (isset($crop) && $crop != "" && $crop != "ALL") {
            $query = $query->andWhere(["=", "agriculture_practice_crop_details.crop_id", $crop]);
        }
        $query->groupBy('agriculture_practice_crop_details.crop_type');
        $command = $query->createCommand();
        $Output = $command->queryAll();
        return $Output;
    }

    // *******
    // Total land_acrease by finnancialYear and $agriculture_practice_id
    public function totalLandAcreaseByAgriPracticeAndFinancialYear($crop_id, $agriculture_practice_id, $FinancialYear) {
        $query = new \yii\db\Query;
        $query->select([
                    'SUM(agriculture_practice_crop_details.land_acreage) as land_acreage_sum'
                ])
                ->from('agriculture_practice_crop_details')
                ->where('agriculture_practice_crop_details.financial_year = ' . $FinancialYear)
                ->andWhere('agriculture_practice_crop_details.agriculture_practice_id = ' . $agriculture_practice_id)
                ->andWhere('agriculture_practice_crop_details.crop_id = ' . $crop_id);
        $command = $query->createCommand();
        $Output = $command->queryAll();
        return $Output[0]['land_acreage_sum'];
    }

    public function getTotalLandAcreaseByCropFinanacialYear($agriculture_practice_id, $FinancialYear, $houseHoldId, $villageId,$cropType) {
        $query = new \yii\db\Query;
//        $query->select([
//                    'crop.crop_name as crop_name',
//                    'SUM(agriculture_practice_crop_details.land_acreage) as land_acreage_sum'
//                ])
//                ->from('agriculture_practice_crop_details')
//                ->innerJoin('crop', 'crop.id = agriculture_practice_crop_details.crop_id')
//                //->where('crop.id = agriculture_practice_crop_details.crop_id')
//                ->where('agriculture_practice_crop_details.financial_year = ' . $FinancialYear)
//                ->andWhere('agriculture_practice_crop_details.agriculture_practice_id = ' . $agriculture_practice_id)
//                ->groupBy('agriculture_practice_crop_details.crop_id');



        $query->select(["agriculture_practice_crop_details.agriculture_practice_id,
                        Sum(agriculture_practice_crop_details.land_acreage) AS agr_pctc_land_acreage_sum,
                        agriculture_practice.practice_name,
                        household_profile.village  as `village_id`,
                        village.village,
                        crop.crop_name as crop_name,
                        SUM(agriculture_practice_crop_details.land_acreage) as land_acreage_sum"])
                ->from('agriculture_practice_crop_details')
                ->innerJoin('agriculture_practice', 'agriculture_practice_crop_details.agriculture_practice_id = agriculture_practice.id')
                ->innerJoin('household_profile', 'agriculture_practice_crop_details.household_id = household_profile.id')
                ->innerJoin('village', 'household_profile.village= village.id')
                ->innerJoin('crop', 'crop.id = agriculture_practice_crop_details.crop_id')
                ->where('agriculture_practice_crop_details.financial_year =  ' . $FinancialYear)
                ->andWhere('agriculture_practice_crop_details.agriculture_practice_id = ' . $agriculture_practice_id);
        if (isset($houseHoldId) && $houseHoldId != "" && $houseHoldId != "ALL") {
            $query = $query->andWhere(["=", "household_profile.id", $houseHoldId]);
        }
        if (isset($villageId) && $villageId != "" && $villageId != "ALL") {
            $query = $query->andWhere(["=", "household_profile.village", $villageId]);
        }
        // suresh
        if (isset($cropType) && $cropType != "" && $cropType != "ALL") {
            $query->andWhere(['agriculture_practice_crop_details.crop_type' => $cropType]);
        }
//                            ->andWhere('household_profile.village = 376')
        //                ->andWhere('household_profile.id = 1')
        $query->groupBy('agriculture_practice_crop_details.crop_id');



        $command = $query->createCommand();
        $Output = $command->queryAll();
        return $Output;
    }

    public function getDataGroupByColumn($table, $column, $grColumn) {
        $query = new \yii\db\Query;
        $query->select([$column])
                ->from($table)
                ->groupBy($grColumn);
        $command = $query->createCommand();
        return $command->queryAll();
    }

    public function getSumOfColumn($FinancialYear, $CropType, $village, $householdId) {
        $query = new \yii\db\Query;
        $query->select(['SUM(land_acreage) as land_acreage_sum'])
                ->from('agriculture_practice_crop_details')
                ->innerJoin('household_profile', 'agriculture_practice_crop_details.household_id = household_profile.id')
                ->where('agriculture_practice_crop_details.financial_year = ' . $FinancialYear)
                ->andWhere('crop_type = "' . $CropType . '"');
        if (isset($householdId) && $householdId != "") {
            $query = $query->andWhere(["=", "household_profile.id", $householdId]);
        }
        if (isset($village) && $village != "") {
            $query = $query->andWhere(["=", "household_profile.village", $village]);
        }

        $command = $query->createCommand();
        $Output = $command->queryAll();
        return $Output[0]['land_acreage_sum'];
    }

    public function getInputDistributionOfFinancialYear($year, $householdId = "", $villageId = "", $cropId = "", $crop_type) {
        $query = new \yii\db\Query;
        $query->select(['id'])
                ->from('agriculture_practice_crop_details')
                ->where('financial_year = ' . $year);
        $command = $query->createCommand();
        $Output = $command->queryAll();
        $agriculturePracticeCropDetailsIdArray = \yii\helpers\ArrayHelper::getColumn($Output, "id");

//        $query = new \yii\db\Query;
//        $query->select([
//                    'SUM( agri_input.fertilizer_urea_quantity * agri_input.fertilizer_urea_rate * agriculture_practice_crop_details.land_acreage) as fertilizer_urea_total',
//                    'SUM( agri_input.pesticides_quantity * agri_input.pesticides_rate * agriculture_practice_crop_details.land_acreage) as pesticides_total',
//                    'SUM( agri_input.bio_pesticides_quantity * agri_input.bio_pesticides_rate * agriculture_practice_crop_details.land_acreage) as bio_pesticides_rate_total',
//                    'SUM( agri_input.fertilizer_zinc_quantity * agri_input.fertilizer_zinc_rate * agriculture_practice_crop_details.land_acreage) as fertilizer_zinc_total',
//                    'SUM( agri_input.fertilizer_quantity * agri_input.fertilizer_rate * agriculture_practice_crop_details.land_acreage) as fertilizer_total',
//                    'SUM( agri_input.farm_yard_manure_quantity * agri_input.farm_yard_manure_rate * agriculture_practice_crop_details.land_acreage) as farm_yard_manure_total',
//                    'SUM( agri_input.green_manure_quantity * agri_input.green_manure_rate * agriculture_practice_crop_details.land_acreage) as green_manure_total',
//                    'SUM( agri_input.bd_compost_quantity * agri_input.bd_compost_rate * agriculture_practice_crop_details.land_acreage) as bd_compost_total',
//                    'SUM( agri_input.irrigation_number_quantity * agri_input.irrigation_number_rate * agriculture_practice_crop_details.land_acreage) as irrigation_number_total',
//                    'SUM( agri_input.seed_rate_quantity * agri_input.seed_rate_rate * agriculture_practice_crop_details.land_acreage) as seed_rate_total',
//                    'SUM( agri_input.weeding_quantity * agri_input.weeding_rate * agriculture_practice_crop_details.land_acreage) as weeding_total',
//                    'SUM( agri_input.permanent_labour_quantity * agri_input.permanent_labour_rate) as permanent_labour_total',
//                    'SUM( agri_input.seasonal_labour_quantity * agri_input.seasonal_labour_rate) as seasonal_labour_total',
//                    // Machine 
//                    'SUM( agri_input.tractor_quantity * agri_input.tractor_rate * agriculture_practice_crop_details.land_acreage) as tractor_total',
//                    'SUM( agri_input.combine_harvester_quantity * agri_input.combine_harvester_rate * agriculture_practice_crop_details.land_acreage) as combine_harvester_total',
//                    'SUM( agri_input.cultivator_quantity * agri_input.cultivator_rate * agriculture_practice_crop_details.land_acreage) as cultivator_total',
//                    'SUM( agri_input.seed_drill_quantity * agri_input.seed_drill_rate * agriculture_practice_crop_details.land_acreage) as seed_drill_total',
//                    'SUM( agri_input.rotavator_quantity * agri_input.rotavator_rate * agriculture_practice_crop_details.land_acreage) as rotavator_total',
//                    'SUM( agri_input.potato_planter_quantity * agri_input.potato_planter_rate * agriculture_practice_crop_details.land_acreage) as potato_planter_total',
//                    'SUM( agri_input.silage_machine_quantity * agri_input.silage_machine_rate * agriculture_practice_crop_details.land_acreage) as silage_machine_total',
//                    'SUM( agri_input.harrow_quantity * agri_input.harrow_rate * agriculture_practice_crop_details.land_acreage) as harrow_total',
//                    'SUM( agri_input.leveler_quantity * leveler_rate * agriculture_practice_crop_details.land_acreage) as leveler_total',
//                    'SUM( agri_input.ridger_quantity * agri_input.ridger_rate * agriculture_practice_crop_details.land_acreage) as ridger_total',
//                    'SUM( agri_input.zero_drill_quantity * agri_input.zero_drill_rate * agriculture_practice_crop_details.land_acreage) as zero_drill_total',
//                    'SUM( agri_input.laser_leveler_quantity * agri_input.laser_leveler_rate * agriculture_practice_crop_details.land_acreage) as laser_leveler_total',
//                    'SUM( agri_input.trolly_quantity * trolly_rate * agriculture_practice_crop_details.land_acreage) as trolly_total',
//                    'SUM( agri_input.generator_quantity * agri_input.generator_rate * agriculture_practice_crop_details.land_acreage) as generator_total',
//                    'SUM( agri_input.mechanical_reaper_quantity * agri_input.mechanical_reaper_rate * agriculture_practice_crop_details.land_acreage) as mechanical_reaper_total',
//                    'SUM( agri_input.others_quantity * agri_input.others_rate * agriculture_practice_crop_details.land_acreage) as others_total',
//                    // end machine
//                    'SUM(agri_input.total_agri_machinery_cost) as total_agri_machinery_cost_sum',
//                    'SUM(agri_input.total_grand_agri_input) as total_grand_agri_input_sum',
//                    'SUM(agri_input.total_agri_input_cost) as total_agri_input_cost'
//                ])
//                ->from('agri_input')
//                ->innerJoin("agriculture_practice_crop_details", "agriculture_practice_crop_details.id=agri_input.agriculture_practice_crop_details_id")
//                ->innerJoin('household_profile', 'agriculture_practice_crop_details.household_id = household_profile.id')
//                ->innerJoin('village', 'household_profile.village= village.id')
//                ->where(['IN', 'agri_input.agriculture_practice_crop_details_id', $agriculturePracticeCropDetailsIdArray])
//                ->andWhere(['agriculture_practice_crop_details.financial_year' => $year]);

                $query = new \yii\db\Query;
        $query->select([
                    'SUM( agri_input.fertilizer_urea_quantity * agri_input.fertilizer_urea_rate) as fertilizer_urea_total',
                    'SUM( agri_input.pesticides_quantity * agri_input.pesticides_rate) as pesticides_total',
                    'SUM( agri_input.bio_pesticides_quantity * agri_input.bio_pesticides_rate) as bio_pesticides_rate_total',
                    'SUM( agri_input.fertilizer_zinc_quantity * agri_input.fertilizer_zinc_rate) as fertilizer_zinc_total',
                    'SUM( agri_input.fertilizer_quantity * agri_input.fertilizer_rate) as fertilizer_total',
                    'SUM( agri_input.farm_yard_manure_quantity * agri_input.farm_yard_manure_rate) as farm_yard_manure_total',
                    'SUM( agri_input.green_manure_quantity * agri_input.green_manure_rate) as green_manure_total',
                    'SUM( agri_input.bd_compost_quantity * agri_input.bd_compost_rate) as bd_compost_total',
                    'SUM( agri_input.irrigation_number_quantity * agri_input.irrigation_number_rate) as irrigation_number_total',
                    'SUM( agri_input.seed_rate_quantity * agri_input.seed_rate_rate) as seed_rate_total',
                    'SUM( agri_input.weeding_quantity * agri_input.weeding_rate) as weeding_total',
                    'SUM( agri_input.permanent_labour_quantity * agri_input.permanent_labour_rate) as permanent_labour_total',
                    'SUM( agri_input.seasonal_labour_quantity * agri_input.seasonal_labour_rate) as seasonal_labour_total',
                    // Machine 
                    'SUM( agri_input.tractor_quantity * agri_input.tractor_rate) as tractor_total',
                    'SUM( agri_input.combine_harvester_quantity * agri_input.combine_harvester_rate) as combine_harvester_total',
                    'SUM( agri_input.cultivator_quantity * agri_input.cultivator_rate) as cultivator_total',
                    'SUM( agri_input.seed_drill_quantity * agri_input.seed_drill_rate) as seed_drill_total',
                    'SUM( agri_input.rotavator_quantity * agri_input.rotavator_rate) as rotavator_total',
                    'SUM( agri_input.potato_planter_quantity * agri_input.potato_planter_rate) as potato_planter_total',
                    'SUM( agri_input.silage_machine_quantity * agri_input.silage_machine_rate) as silage_machine_total',
                    'SUM( agri_input.harrow_quantity * agri_input.harrow_rate) as harrow_total',
                    'SUM( agri_input.leveler_quantity * leveler_rate) as leveler_total',
                    'SUM( agri_input.ridger_quantity * agri_input.ridger_rate) as ridger_total',
                    'SUM( agri_input.zero_drill_quantity * agri_input.zero_drill_rate) as zero_drill_total',
                    'SUM( agri_input.laser_leveler_quantity * agri_input.laser_leveler_rate) as laser_leveler_total',
                    'SUM( agri_input.trolly_quantity * trolly_rate) as trolly_total',
                    'SUM( agri_input.generator_quantity * agri_input.generator_rate) as generator_total',
                    'SUM( agri_input.mechanical_reaper_quantity * agri_input.mechanical_reaper_rate) as mechanical_reaper_total',
                    'SUM( agri_input.others_quantity * agri_input.others_rate) as others_total',
                    // end machine
                    'SUM(agri_input.total_agri_machinery_cost) as total_agri_machinery_cost_sum',
                    'SUM(agri_input.total_grand_agri_input) as total_grand_agri_input_sum',
                    'SUM(agri_input.total_agri_input_cost) as total_agri_input_cost'
                ])
                ->from('agri_input')
                ->innerJoin("agriculture_practice_crop_details", "agriculture_practice_crop_details.id=agri_input.agriculture_practice_crop_details_id")
                ->innerJoin('household_profile', 'agriculture_practice_crop_details.household_id = household_profile.id')
                ->innerJoin('village', 'household_profile.village= village.id')
                ->where(['IN', 'agri_input.agriculture_practice_crop_details_id', $agriculturePracticeCropDetailsIdArray])
                ->andWhere(['agriculture_practice_crop_details.financial_year' => $year]);
        if (isset($householdId) && $householdId != "" && $householdId != "ALL") {
            $query = $query->andWhere(["=", "household_profile.id", $householdId]);
        }
        if (isset($villageId) && $villageId != "" && $villageId != "ALL") {
            $query = $query->andWhere(["=", "household_profile.village", $villageId]);
        }
        if (isset($cropId) && $cropId != "" && $cropId != "ALL") {
            $query = $query->andWhere(["=", "agriculture_practice_crop_details.crop_id", $cropId]);
        }
        if ($crop_type) {
            $query->andWhere(['agriculture_practice_crop_details.crop_type' => $crop_type]);
        }
        $command = $query->createCommand();
        $Output = $command->queryAll();
        return $Output[0];
    }

    // Cattle

    public function getTotalCattleCount($FinancialYear, $villageId, $houseHoldId, $cattleEntryType) {
        $groupByCattleType = "";
        $query = new \yii\db\Query;
        $query->select(["cattle_profile.cattle_type AS cattle_type,
                        Count(*) AS cattleCount"])
                ->from("cattle_profile")
                ->innerJoin("household_profile", "cattle_profile.household_id = household_profile.id");
        if (isset($FinancialYear) && $FinancialYear != "" && $FinancialYear != "ALL") {
            $query = $query->where('YEAR(CAST(cattle_profile.create_on AS DATE)) =  ' . $FinancialYear);
        }
        if (isset($villageId) && $villageId != "" && $villageId != "ALL") {
            $query = $query->andWhere(["=", "household_profile.village", $villageId]);
        }
        if (isset($houseHoldId) && $houseHoldId != "" && $houseHoldId != "ALL") {
            $query = $query->andWhere(["=", "cattle_profile.household_id", $houseHoldId]);
        }
        if (isset($cattleEntryType) && $cattleEntryType != "" && $cattleEntryType != "ALL") {
            $query = $query->andWhere(["=", "cattle_profile.cattle_entry_type", $cattleEntryType]);
            $groupByCattleType = " , cattle_entry_type";
        }

        $query->groupBy("cattle_type" . $groupByCattleType);
        $command = $query->createCommand();
        return $command->queryAll();
    }

    public function cattleBreed($cattleType, $FinancialYear, $villageId, $houseHoldId, $cattleEntryType) {
//        $query = new \yii\db\Query;
//        $query->select(['COUNT(`breed`) as countBreed', 'breed'])
//                ->from('cattle_profile')
//                ->where(['cattle_type' => $cattleType])
//                ->groupBy('breed');

        $groupByCattleType = "";
        $query = new \yii\db\Query;
        $query->select(["cattle_profile.cattle_type AS cattle_type,
                        breed,
                        Count(*) AS countBreed"])
                ->from("cattle_profile")
                ->innerJoin("household_profile", "cattle_profile.household_id = household_profile.id")
                ->where(['cattle_profile.cattle_type' => $cattleType]);

        if (isset($FinancialYear) && $FinancialYear != "" && $FinancialYear != "ALL") {
            $query = $query->andWhere(["=", "YEAR(CAST(cattle_profile.create_on AS DATE))", $FinancialYear]);
        }
        if (isset($villageId) && $villageId != "" && $villageId != "ALL") {
            $query = $query->andWhere(["=", "household_profile.village", $villageId]);
        }
        if (isset($houseHoldId) && $houseHoldId != "" && $houseHoldId != "ALL") {
            $query = $query->andWhere(["=", "cattle_profile.household_id", $houseHoldId]);
        }
        if (isset($cattleEntryType) && $cattleEntryType != "" && $cattleEntryType != "ALL") {
            $query = $query->andWhere(["=", "cattle_profile.cattle_entry_type", $cattleEntryType]);
            $groupByCattleType = " , cattle_entry_type";
        }

        $query->groupBy("breed" . $groupByCattleType);
        $command = $query->createCommand();
        $Output = $command->queryAll();
        return $Output;
    }

    public function dairyExpensePerHousehold($financialYear) {
        $query = new \yii\db\Query;
        $query->select([
                    'SUM(feed_expense) as feed_expense',
                    'SUM(green_fodder_expense) as green_fodder_expense',
                    'SUM(dry_matter_expense) as dry_matter_expense',
                    'SUM(mineral_mixture_expense) as mineral_mixture_expense',
                    'SUM(medicine_homebased_expense) as medicine_homebased_expense',
                    'SUM(medicine_allopathy_expense) as medicine_allopathy_expense',
                    'SUM(medicine_homeopathy_expense) as medicine_homeopathy_expense',
                    'SUM(vaccination_expense) as vaccination_expense',
                    'SUM(ai_expense) as ai_expense',
                    'SUM(cattle_purchase_expense) as cattle_purchase_expense',
                    'SUM(cattle_death_expense) as cattle_death_expense'
                ])
                ->from('dairy_expense')
                ->where(['LIKE', 'month_year', '-' . $financialYear]);
        $command = $query->createCommand();
        $Output = $command->queryAll();
        return $Output[0];
    }

    public function householdCount($financialYear) {
        $query = new \yii\db\Query;
        $query->select([
                    'COUNT(household_id) as householdCount'
                ])
                ->from('dairy_expense')
                ->where(['LIKE', 'month_year', '-' . $financialYear]);
        $command = $query->createCommand();
        $Output = $command->queryAll();
        return $Output[0];
    }

    public function getKitchenGraphFinancialYear() {
        $queryKG = new \yii\db\Query;
        $queryKG->select(["ROUND(SUM(kitchen_garden.area) / COUNT(kitchen_garden.area),2) AS `Area`,
                                   SUBSTRING_INDEX(SUBSTRING_INDEX(`month_year`, '-', - 1),'-',2) AS `Year`"])
                ->from('kitchen_garden')
                ->groupBy(["SUBSTRING_INDEX(SUBSTRING_INDEX(`month_year`, '-', -1 ),'-',2)"]);
        $commandKG = $queryKG->createCommand();
        $opKg = $commandKG->queryAll();
        $array = array();
        for ($i = 0; $i < count($opKg); $i++) {
            $array["year"][] = $opKg[$i]["Year"];
            $array["area"][] = $opKg[$i]["Area"];
        }
        return $array;
    }

    public function getCropProductionFinancialYear($villageId, $houseHoldId, $cropId) {
        $query = new \yii\db\Query;
//        $query->select(["agriculture_practice_crop_details.crop_type,
//                                    agriculture_practice_crop_details.financial_year,
//                                    SUM(agri_output.production) as `production`,
//                                    COUNT(*) as `number_of_crop`,
//                                    ROUND(SUM(agri_output.production) / COUNT(*)) as `average_production`"])
//                ->from('agriculture_practice_crop_details')
//                ->innerJoin("agri_output", "agri_output.agriculture_practice_crop_details_id = agriculture_practice_crop_details.id")
//                ->groupBy(["financial_year", "agriculture_practice_crop_details.crop_type"]);
        $query->select(["agriculture_practice_crop_details.crop_type,
                    agriculture_practice_crop_details.financial_year,
                    if(SUM(agri_output.production) is null or SUM(agri_output.production) = '', 0 ,SUM(agri_output.production)) as `production`,
                    IF(COUNT(*) is null or COUNT(*) ='',0,(COUNT(*) )) as `number_of_crop`,
                    IF(ROUND(SUM(agri_output.production) / COUNT(*)) IS NULL or ROUND(SUM(agri_output.production) / COUNT(*)) = '', 0 , ROUND(SUM(agri_output.production) / COUNT(*))) as average_production "])
                ->from('agriculture_practice_crop_details')
                ->innerJoin("agri_output", "agri_output.agriculture_practice_crop_details_id = agriculture_practice_crop_details.id")
                ->innerJoin("household_profile", "agriculture_practice_crop_details.household_id = household_profile.id")
                ->innerJoin("crop", "agriculture_practice_crop_details.crop_id = crop.id");
        if (isset($villageId) && $villageId != "") {
            $query = $query->where(['household_profile.village' => $villageId]);
        }
        if (isset($houseHoldId) && $houseHoldId != "") {
            $query = $query->andWhere(["=", "household_profile.id", $houseHoldId]);
        }
        if (isset($cropId) && $cropId != "") {
            $query = $query->andWhere(["=", "agriculture_practice_crop_details.crop_id", $cropId]);
        }
        $query->groupBy("financial_year,agriculture_practice_crop_details.crop_type");
        $command = $query->createCommand();
        $op = $command->queryAll();
        return $op;
    }

    public function milkingDataAtProjectLevel($financialYear, $villageId = "", $houseHoldId = "",$cattleId="") {
        $connection = \Yii::$app->getDb();
        $varIn = '(' .
                '"' . 'January-' . $financialYear . '",' .
                '"' . 'February-' . $financialYear . '",' .
                '"' . 'March-' . $financialYear . '",' .
                '"' . 'April-' . $financialYear . '",' .
                '"' . 'May-' . $financialYear . '",' .
                '"' . 'June-' . $financialYear . '",' .
                '"' . 'July-' . $financialYear . '",' .
                '"' . 'August-' . $financialYear . '",' .
                '"' . 'September-' . $financialYear . '",' .
                '"' . 'October-' . $financialYear . '",' .
                '"' . 'November-' . $financialYear . '",' .
                '"' . 'December-' . $financialYear . '"' .
                ")";
        $strVillage = "";
        $strHouseHold = "";
        $cattleIdForm ="";
        if (isset($villageId) && $villageId != "" && $villageId != "ALL") {
            $strVillage .= " and household_profile.village = " . $villageId;
        }
        if (isset($houseHoldId) && $houseHoldId != "" && $houseHoldId != "ALL") {
            $strHouseHold .= " and household_profile.id =  " . $houseHoldId;
        }
        if (isset($cattleId) && $cattleId != "" && $cattleId != "ALL") {
            $cattleIdForm .= " and milk_production.cattle_profile_id =  " . $cattleId;
        }
        $command = $connection->createCommand("SELECT `month_year`, SUM(`total`) as `quantity` FROM `milk_production` INNER JOIN household_profile ON milk_production.household_id = household_profile.id  INNER JOIN cattle_profile ON milk_production.cattle_profile_id = cattle_profile.id  WHERE `month_year` IN " . $varIn . $strVillage . $strHouseHold . $cattleIdForm."  GROUP BY `month_year` ORDER BY STR_TO_DATE(`month_year`, '%M-%Y') ASC");
        $result = $command->queryAll();
        return $result;
    }
    public function cattlewiseMilkingProduction($financialYear, $villageId = "", $houseHoldId = "",$cattleId="") {
        $connection = \Yii::$app->getDb();
        $varIn = '(' .
                '"' . 'January-' . $financialYear . '",' .
                '"' . 'February-' . $financialYear . '",' .
                '"' . 'March-' . $financialYear . '",' .
                '"' . 'April-' . $financialYear . '",' .
                '"' . 'May-' . $financialYear . '",' .
                '"' . 'June-' . $financialYear . '",' .
                '"' . 'July-' . $financialYear . '",' .
                '"' . 'August-' . $financialYear . '",' .
                '"' . 'September-' . $financialYear . '",' .
                '"' . 'October-' . $financialYear . '",' .
                '"' . 'November-' . $financialYear . '",' .
                '"' . 'December-' . $financialYear . '"' .
                ")";
        $strVillage = "";
        $strHouseHold = "";
        $cattleIdForm ="";
        if (isset($villageId) && $villageId != "" && $villageId != "ALL") {
            $strVillage .= " and household_profile.village = " . $villageId;
        }
        if (isset($houseHoldId) && $houseHoldId != "" && $houseHoldId != "ALL") {
            $strHouseHold .= " and household_profile.id =  " . $houseHoldId;
        }
        if (isset($cattleId) && $cattleId != "" && $cattleId != "ALL") {
            $cattleIdForm .= " and milk_production.cattle_profile_id =  " . $cattleId;
        }
        $command = $connection->createCommand("SELECT `month_year`, SUM(`total`) as `quantity`, ROUND(SUM(`total`)/COUNT(`total`),2) as `average` FROM `milk_production` INNER JOIN household_profile ON milk_production.household_id = household_profile.id  INNER JOIN cattle_profile ON milk_production.cattle_profile_id = cattle_profile.id  WHERE `month_year` IN " . $varIn . $strVillage . $strHouseHold . $cattleIdForm." and milking_status='Milking' GROUP BY `month_year` ORDER BY STR_TO_DATE(`month_year`, '%M-%Y') ASC");
        $result = $command->queryAll();
        return $result;
    }

//    public function cattleCount() {
//        $query = new \yii\db\Query;
//        $query->select([
//                    'COUNT(id) as cattleCount'
//                ])
//                ->from('cattle_profile');
//        $command = $query->createCommand();
//        $Output = $command->queryAll();
//        return $Output[0];
//    }
}
