<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProgramDetails;

/**
 * ProgramDetailsSearch represents the model behind the search form about `app\models\ProgramDetails`.
 */
class ProgramDetailsSearch extends ProgramDetails
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'set', 'number_of_product', 'total', 'product_id'], 'integer'],
            [['program_name', 'month', 'program_code', 'color', 'entry_by', 'created_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProgramDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'set' => $this->set,
            'number_of_product' => $this->number_of_product,
            'total' => $this->total,
            'product_id' => $this->product_id,
            'created_on' => $this->created_on,
        ]);

        $query->andFilterWhere(['like', 'program_name', $this->program_name])
            ->andFilterWhere(['like', 'month', $this->month])
            ->andFilterWhere(['like', 'program_code', $this->program_code])
            ->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'entry_by', $this->entry_by]);

        return $dataProvider;
    }
}
