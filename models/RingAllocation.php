<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ring_allocation".
 *
 * @property integer $id
 * @property integer $program_detail_id
 * @property integer $employee_id
 * @property string $ring_type
 * @property string $ring_color
 * @property string $no_of_ring
 * @property string $ring_weight
 * @property string $on_date
 * @property string $entry_by
 * @property string $created_on
 */
class RingAllocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ring_allocation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['program_detail_id', 'employee_id'], 'integer'],
            [['on_date', 'created_on'], 'safe'],
            [['ring_type', 'ring_color', 'no_of_ring', 'entry_by'], 'string', 'max' => 30],
            [['ring_weight'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'program_detail_id' => 'Program Detail ID',
            'employee_id' => 'Employee ID',
            'ring_type' => 'Ring Type',
            'ring_color' => 'Ring Color',
            'no_of_ring' => 'No Of Ring',
            'ring_weight' => 'Ring Weight',
            'on_date' => 'On Date',
            'entry_by' => 'Entry By',
            'created_on' => 'Created On',
        ];
    }
}
