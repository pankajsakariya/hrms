<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "group".
 *
 * @property integer $id
 * @property string $group_area_name
 * @property string $employee_id
 * @property string $created_on
 * @property string $enter_by
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_on'], 'safe'],
            [['group_area_name', 'employee_id', 'enter_by'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_area_name' => 'Group Area Name',
            'employee_id' => 'Area Owner',
            'created_on' => 'Created On',
            'enter_by' => 'Enter By',
        ];
    }
}
