<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "area".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $area_name
 * @property string $area_code
 * @property string $created_on
 * @property string $enter_by
 */
class Area extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id'], 'integer'],
            [['created_on'], 'safe'],
            [['area_name', 'area_code', 'enter_by'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'area_name' => 'Area Name',
            'area_code' => 'Area Code',
            'created_on' => 'Created On',
            'enter_by' => 'Enter By',
        ];
    }
}
