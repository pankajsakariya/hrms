<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property integer $id
 * @property integer $area_id
 * @property integer $group_id
 * @property string $employee_name
 * @property string $mobile
 * @property string $address
 * @property string $aadhar_no
 * @property string $bank_account_no
 * @property string $bank_name
 * @property string $ifsc_code
 * @property string $id_proof_photo
 * @property string $employee_photo
 * @property integer $reference
 * @property string $status
 * @property string $created_on
 * @property string $enter_by
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['area_id', 'group_id', 'mobile', 'reference'], 'integer'],
            [['created_on'], 'safe'],
            [['employee_name', 'aadhar_no', 'bank_account_no', 'bank_name', 'ifsc_code', 'id_proof_photo', 'employee_photo', 'enter_by'], 'string', 'max' => 30],
            [['address'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 12],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area_id' => 'Area',
            'group_id' => 'Group',
            'employee_name' => 'Employee Name',
            'mobile' => 'Mobile',
            'address' => 'Address',
            'aadhar_no' => 'Aadhar No',
            'bank_account_no' => 'Bank Account No',
            'bank_name' => 'Bank Name',
            'ifsc_code' => 'Ifsc Code',
            'id_proof_photo' => 'Id Proof Photo',
            'employee_photo' => 'Employee Photo',
            'reference' => 'Reference',
            'status' => 'Status',
            'created_on' => 'Created On',
            'enter_by' => 'Enter By',
        ];
    }
}
