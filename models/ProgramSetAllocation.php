<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "program_set_allocation".
 *
 * @property integer $id
 * @property string $date
 * @property integer $employee_id
 * @property integer $program_detail_id
 * @property string $product_color
 * @property string $given_material_gross_weight
 * @property integer $number_of_product
 * @property string $ring_status
 * @property string $last_material
 * @property string $entry_by
 * @property string $created_on
 */
class ProgramSetAllocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'program_set_allocation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'created_on'], 'safe'],
            [['employee_id', 'program_detail_id', 'number_of_product'], 'integer'],
            [['product_color', 'given_material_gross_weight', 'ring_status', 'entry_by'], 'string', 'max' => 30],
            [['last_material'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'employee_id' => 'Employee ID',
            'program_detail_id' => 'Program Detail ID',
            'product_color' => 'Product Color',
            'given_material_gross_weight' => 'Given Material Gross Weight',
            'number_of_product' => 'Number Of Product',
            'ring_status' => 'Ring Status',
            'last_material' => 'Last Material',
            'entry_by' => 'Entry By',
            'created_on' => 'Created On',
        ];
    }
}
