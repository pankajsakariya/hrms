<?php

use app\models\UserLevels;
use yii\rest\Controller;
use yii\widgets\ActiveForm;

function hideGetParams() {
    ?>
    <script>
        $(document).ready(function () {
            if (typeof window.history.pushState === 'function') {
                window.history.pushState({}, "Hide", '<?php echo $_SERVER['PHP_SELF']; ?>');
            }
        });
    </script>
    <?php
}

function redirectWithPage($mthis, $arrayM) {

    if (isset($_SESSION['page']['controller']) &&
            $_SESSION['page']['controller'] == Yii::$app->controller->id &&
            isset($_SESSION['page']['page_number'])) {

        $arrayM['page'] = $_SESSION['page']['page_number'];
    }

    return $mthis->redirect($arrayM);
}

function pageNumberMapper($dataProvider) {

    $_SESSION['page'] = array();
    $_SESSION['page']['controller'] = Yii::$app->controller->id;
    $_SESSION['page']['action'] = Yii::$app->controller->action->id;
    $_SESSION['page']['page_number'] = $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : '1';
}

function isTesting() {
    return false;
}

function excepPrintVariableGenerator() {
    
}

function hrefPost() {
    ?>

    <?php
//    $action = \yii\helpers\Url::to([$parameters['r']]);
    $action = "index.php";
    ?>

    <?php $form = ActiveForm::begin(['method' => "POST", 'id' => 'sf', 'action' => $action]); ?>
    <?php
    foreach ($parameters as $key => $value) {
        ?>
        <textarea name="<?php echo $key ?>" ><?php echo $value; ?></textarea>
        <?php
    }
    ?>
    <?php ActiveForm::end(); ?>

    <script>
        document.getElementById("sf").submit();
    </script>

    <?php
}

function redirectWithMethod($method = "GET", $parametersAsMethod = [], $parameterInURL = []) {
    ?>
    <div style="background: #000000;">
        <div style="display: none">

            <?php
            $action = \yii\helpers\Url::to($parameterInURL);
            ?>
            <?php $form = ActiveForm::begin(['method' => "POST", 'id' => 'sf', 'action' => $action]); ?>
            <?php
            foreach ($parametersAsMethod as $key => $value) {
                ?>
                <textarea name="<?php echo $key ?>" ><?php echo $value; ?></textarea>
                <?php
            }
            ?>
            <?php ActiveForm::end(); ?>

            <script>
                document.getElementById("sf").submit();
            </script>
        </div>
    </div>
    <?php
}

function checkCurrentdateIsBetweenTwoDates($start_date, $end_date) {
    if ($start_date != "" && $end_date != "") {
        $paymentDate = date('Y-m-d');
        $paymentDate = date('Y-m-d', strtotime($paymentDate));
        ;
        //echo $paymentDate; // echos today! 
        $contractDateBegin = date('Y-m-d', strtotime($start_date));
        $contractDateEnd = date('Y-m-d', strtotime($end_date));

        if (($paymentDate > $contractDateBegin) && ($paymentDate < $contractDateEnd)) {
            return true;
        } else {
            return false;
        }
    }
    return false;
}

function checkCurrentdateIsGreaterSomeDate($end_date) {
    if ($end_date != "") {
        $paymentDate = date('Y-m-d');
        $paymentDate = date('Y-m-d', strtotime($paymentDate));
        ;
        //echo $paymentDate; // echos today! 
        $contractDateEnd = date('Y-m-d', strtotime($end_date));

        if (($paymentDate > $contractDateEnd)) {
            return true;
        } else {
            return false;
        }
    }
    return false;
}

function checkCurrentdateIsBeforeSomeDate($start_date) {
    if ($start_date != "") {
        $paymentDate = date('Y-m-d');
        $paymentDate = date('Y-m-d', strtotime($paymentDate));
        ;
        //echo $paymentDate; // echos today! 
        $contractDateBegin = date('Y-m-d', strtotime($start_date));

        if (($paymentDate < $contractDateBegin)) {
            return true;
        } else {
            return false;
        }
    }
    return false;
}

function getCurrentDatePositionFromStartAndEnd($start_date, $end_date) {

    if (checkCurrentdateIsBetweenTwoDates($start_date, $end_date) == true) {
        return "Ongoing";
    } else if (checkCurrentdateIsGreaterSomeDate($end_date) == true) {
        return "Completed";
    } else if (checkCurrentdateIsBeforeSomeDate($start_date) == true) {
        return "Not Started";
    }
}

function fillUserData($model, $column_name) {
    if (!$model->isNewRecord) {
        $pagerAuthenticationModels = app\models\PageAuthentications::find()->where(['level_id' => $model->id])->all();
        $columnValues = yii\helpers\ArrayHelper::getColumn($pagerAuthenticationModels, $column_name);
        return $columnValues;
    }
    return [];
}

function hideFields($model, $fieldsArray) {
    ?>

    <script>
        $(document).ready(function ()
        {

    <?php
//            $fieldsArray =  $model->nsdcFields();

    for ($i = 0; $i < count($fieldsArray); $i++) {
        ?>
                $(".field-<?php echo getCurrentControllerNameForFieldContainer() ?>-<?php echo $fieldsArray[$i] ?>").hide();
        <?php
    }
    ?>



            //        alert("Sdsds");
            //        var fields_containers =  $("div[class*='form-group field']");
            //        
            //        $("div[class*='form-group field']").filter(function(index)
            //        {
            //            alert($(this).);
            //        })

        })
    </script>
    <?php
}

function createUrls($actions, $module) {
    $actionssm = array();

    for ($i = 0; $i < count($actions); $i++) {
        $actionssm[$module . "/" . $actions[$i]] = ucfirst($actions[$i]);
    }

    return $actionssm;
}

function getNotifyTo() {
    $action = Yii::$app->urlManager->parseRequest(Yii::$app->request);

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }


    if (isset($_SESSION['login_info'])) {
        $loginModel = unserialize(serialize($_SESSION['login_info']));

        $pageAuthentications = app\models\PageAuthentications::find()
                ->where(['level_id' => $loginModel->user_level, 'authentication' => $action[0]])
                ->one();
        if ($pageAuthentications != null) {
            return $pageAuthentications->notify_to;
        }
    }

    return '';
}

function getApproveTo() {


    $action = Yii::$app->urlManager->parseRequest(Yii::$app->request);

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }


    if (isset($_SESSION['login_info'])) {
        $loginModel = unserialize(serialize($_SESSION['login_info']));

        $pageAuthentications = app\models\PageAuthentications::find()
                ->where(['level_id' => $loginModel->user_level, 'authentication' => $action[0]])
                ->one();
        if ($pageAuthentications != null) {
            return $pageAuthentications->approve_to;
        }
    }

    return '';
}

function isUnderTrainning($model) {
    if (isset($model->graduation_status) && $model->graduation_status != "") {
        $returnwhat = true;
    }
}

function checkUserHaveToNotifySomeOne() {


    $action = Yii::$app->urlManager->parseRequest(Yii::$app->request);

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }


    if (isset($_SESSION['login_info'])) {
        $loginModel = unserialize(serialize($_SESSION['login_info']));

        $pageAuthentications = app\models\PageAuthentications::find()
                ->where(['level_id' => $loginModel->user_level, 'authentication' => $action[0]])
                ->andWhere(['<>', 'notify_to', ''])
                ->one();
        if ($pageAuthentications != null) {
            return true;
        }
    }

    return false;
}

function checkUserHaveToApproveSomeOne() {


    $action = Yii::$app->urlManager->parseRequest(Yii::$app->request);

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }


    if (isset($_SESSION['login_info'])) {
        $loginModel = unserialize(serialize($_SESSION['login_info']));

        $pageAuthentications = app\models\PageAuthentications::find()
                ->where(['level_id' => $loginModel->user_level, 'authentication' => $action[0]])
                ->andWhere(['<>', 'approve_to', ''])
                ->one();
        if ($pageAuthentications != null) {
            return true;
        }
    }

    return false;
}

function modelSave($model) {

    if (isset($model->id)) {
        $modelId = $model->id;
    } else if (isset($model->center_id)) {
        $modelId = $model->center_id;
    } else if (isset($model->district_id)) {
        $modelId = $model->district_id;
    }

    if ($model->validate() == false) {
        return false;
    }
    if (checkUserHaveToApproveSomeOne() == true) {



        $approvalModel = new app\models\Approval();
        $approvalModel->message = ucfirst(explode("/", $_GET["r"])[0]) . " " . ucfirst(explode("/", $_GET["r"])[1]);
        $approvalModel->model_string = json_encode($_POST);
        $approvalModel->action = explode("?", $_SERVER["REQUEST_URI"])[1];

        date_default_timezone_set('Asia/Calcutta');
        $approvalModel->datem = date("Y-m-d h:i:s");
        $approvalModel->status = "0";
        $approvalModel->to = getApproveTo();

        if (checkUserHaveToNotifySomeOne() == true) {
            $approvalModel->notify_to = getNotifyTo();
        }

        $approvalModel->save();



        return true;
    } else {
        if ($model->save()) {
            if (isset($_POST['approve_id'])) {

                $approveModel = \app\models\Approval::find()->where(['id' => $_POST['approve_id']])->one();

                $approveModel->status = "1";

                $approveModel->save();

                if ($_POST['notify_to'] != "") {

                    $notifyArray = json_decode($_POST['notify_to'], FALSE);

                    for ($kk = 0; $kk < count($notifyArray); $kk++) {

                        $notifyModel = new app\models\Notifications();


                        date_default_timezone_set('Asia/Calcutta');
                        $notifyModel->message = $_POST['message'];
                        $notifyModel->action = explode("%2F", $_POST['action'])[0] . "/view&id=" . $modelId;
                        $notifyModel->to = $notifyArray[$kk];
                        $notifyModel->datem = date("Y-m-d h:i:s");
                        $notifyModel->save();
                    }
                }
            }
            if (checkUserHaveToNotifySomeOne() == true) {
                $action1 = explode("?", $_SERVER["REQUEST_URI"])[1];


                $notifyArray = json_decode(getNotifyTo(), FALSE);

                for ($kk = 0; $kk < count($notifyArray); $kk++) {

                    $notifyModel = new app\models\Notifications();


                    date_default_timezone_set('Asia/Calcutta');
                    $notifyModel = new app\models\Notifications();
                    $notifyModel->message = ucfirst(explode("/", $_GET["r"])[0]) . " " . ucfirst(explode("/", $_GET["r"])[1]);
                    $notifyModel->action = explode("%2F", $action1)[0] . "/view&id=" . $modelId;
                    $notifyModel->to = $notifyArray[$kk];
                    $notifyModel->datem = date("Y-m-d h:i:s");
                    $notifyModel->save();
                }

//                $notifyModel = new app\models\Notifications();
//                $notifyModel->message = ucfirst(explode("/", $_GET["r"])[0])." ".ucfirst(explode("/", $_GET["r"])[1]);
//                $notifyModel->action = explode("%2F", $action1)[0]."/view&id=".$model->id;
//                $notifyModel->to = getNotifyTo();
//                $notifyModel->datem = date("Y-m-d h:i:s");
//                $notifyModel->save();
            }
            return true;
        } else {
            return false;
        }
    }
}

function checkDisability($compareField, $dataProviderDataSingle, $compareFieldAfter) {
    if ($compareField != "") {
        if ($dataProviderDataSingle->$compareField == "") {
            return "type='hidden' value=''";
        }
    }
    if ($compareFieldAfter != "") {
        if ($dataProviderDataSingle->$compareFieldAfter == "1") {
            return "type='hidden' value='1'";
//            return "type='hidden'";
        }
    }
    return "";
}

function fillDataTextField($returnArrayCandidate, $dataProviderDataSingleID, $field_name, $dataProviderDataSingle = null) {
    if (isset($returnArrayCandidate[$dataProviderDataSingleID]['Candidate'][$field_name])) {
        return $returnArrayCandidate[$dataProviderDataSingleID]['Candidate'][$field_name];
    } else if ($dataProviderDataSingle != null) {
        return $dataProviderDataSingle->$field_name;
    }

    return "";
}

function fillDataCheckBox($returnArrayCandidate, $dataProviderDataSingleID, $field_name, $dataProviderDataSingle = null) {
    if (isset($returnArrayCandidate[$dataProviderDataSingleID]['Candidate'][$field_name])) {
        if ($returnArrayCandidate[$dataProviderDataSingleID]['Candidate'][$field_name] == "1") {
            return "checked";
        }
    } else if ($dataProviderDataSingle != null) {
        if ($dataProviderDataSingle->$field_name == "1") {
            return "checked";
        }
    }


    return "";
}

function checkError($returnArrayCandidate, $dataProviderDataSingleID, $field_name) {
    if (isset($returnArrayCandidate[$dataProviderDataSingleID]['errors'][$field_name])) {
        return $returnArrayCandidate[$dataProviderDataSingleID]['errors'][$field_name][0];
    }
    return "";
}

function getPrinatableArray($fieldsToShow, $dataProvider, $dataProviderModified = array()) {
    $dataproviderArray = $dataProvider->models;

    $labels = $dataproviderArray[0]->attributeLabels();

    $printableArray = array();

    $printableArray[0][0] = "S No";
    for ($j = 0; $j < count($fieldsToShow); $j++) {
        $printableArray[0][$j + 1] = $labels[$fieldsToShow[$j]];
    }

    for ($i = 0; $i < count($dataproviderArray); $i++) {
        $printableArray[$i + 1][0] = $i + 1;
        for ($j = 0; $j < count($fieldsToShow); $j++) {
            if (isset($dataProviderModified[$i][$fieldsToShow[$j]])) {
                $printableArray[$i + 1][$j + 1] = $dataProviderModified[$i][$fieldsToShow[$j]];
            } else {
                $printableArray[$i + 1][$j + 1] = $dataproviderArray[$i]->$fieldsToShow[$j];
            }
        }
    }

    return $printableArray;
}

function exportCSV($fieldsToShow, $dataProvider, $dataProviderModified = array()) {

//    $fieldsToShow =array();
//    $fieldsToShow[] = "id";
//    $fieldsToShow[] = "state_name";


    $dataproviderArray = $dataProvider->models;

    $labels = $dataproviderArray[0]->attributeLabels();

    $printableArray = array();

    $printableArray[0][0] = "S No";
    for ($j = 0; $j < count($fieldsToShow); $j++) {
        $printableArray[0][$j + 1] = $labels[$fieldsToShow[$j]];
    }

    for ($i = 0; $i < count($dataproviderArray); $i++) {
        $printableArray[$i + 1][0] = $i + 1;
        for ($j = 0; $j < count($fieldsToShow); $j++) {
            if (isset($dataProviderModified[$i][$fieldsToShow[$j]])) {
                $printableArray[$i + 1][$j + 1] = $dataProviderModified[$i][$fieldsToShow[$j]];
            } else {
                $printableArray[$i + 1][$j + 1] = $dataproviderArray[$i]->$fieldsToShow[$j];
            }
        }
    }

    $list = $printableArray;


    $fp = fopen('file.csv', 'w');

    foreach ($list as $fields) {
        fputcsv($fp, $fields);
    }



    $file = 'file.csv';

    if (file_exists($file)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
    }
}

function changeDateformatWhileSave($model) {
    if (isset($_POST[getCurrentControllerNameFromId()])) {

        $dbColumns = $model->getTableSchema()->columns;
        foreach ($dbColumns as $key => $value) {
            if ($value->type == "date") {
                if (isset($_POST[getCurrentControllerNameFromId()][$key])) {
                    if ($_POST[getCurrentControllerNameFromId()][$key] != "") {
//                        display_array($_POST[getCurrentControllerNameFromId()][$key]);
//                        exit;
//                        display_array($value);
                        $_POST[getCurrentControllerNameFromId()][$key] = changeDateFormat($_POST[getCurrentControllerNameFromId()][$key]);
                    }
                }
            }
        }
    }
}

function changeDateformatWhileGet($model) {
    $modelColumnData = $model->attributes;
    $dbColumns = $model->getTableSchema()->columns;
    foreach ($dbColumns as $key => $value) {
        if ($value->type == "date") {
            if (isset($modelColumnData[$key])) {
                if ($modelColumnData[$key] != "") {
                    $modelColumnData[$key] = changeDateFormat($modelColumnData[$key], 'Y-m-d', 'd-m-Y');
                }
            }
        }
    }
    $model->attributes = $modelColumnData;
    return $model;
}

function getCurrentControllerNameFromId() {
    $controllerName = Yii::$app->controller->id;
    $controllerName = str_replace("-", " ", $controllerName);
    $controllerName = ucwords($controllerName);
    $controllerName = str_replace(" ", "", $controllerName);

    return $controllerName;
}

function getCurrentControllerNameForFieldContainer() {
    $controllerName = Yii::$app->controller->id;
    $controllerName = str_replace("-", "", $controllerName);
    return $controllerName;
}

function changeDateFormat($dateString, $inputFormat = 'd-m-Y', $outputFormat = 'Y-m-d') {
    $myDateTime = \DateTime::createFromFormat($inputFormat, $dateString);
    $newDateString = $dateString;
    if ($myDateTime != "") {
        $newDateString = $myDateTime->format($outputFormat);
    } else {
        $newDateString = "";
//        display_array($dateString);exit;
    }

    return $newDateString;
}

function arrayToDoubleArray($array) {
    $doubleArray = array();
    foreach ($array as &$value) {
        $value = trim($value, ' ');
        $doubleArray[$value] = $value;
    }
    return $doubleArray;
}

function display_array($array) {
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}


function checkAndGetDependant() {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }


    if (isset($_SESSION['login_info'])) {

        $loginModel = unserialize(serialize($_SESSION['login_info']));
        $dependentModel = \app\models\Dependants::find()->where(['users_id' => $loginModel->id])->one();

//        display_array($loginModel->id);
//        exit;

        return $dependentModel;
    }
    return null;
}

function getCurrentUserId() {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    if (isset($_SESSION['login_info'])) {

        $loginModel = unserialize(serialize($_SESSION['login_info']));
        return $loginModel->id;
    }
    return "";
}

function checkIsAdmin() {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    if (isset($_SESSION['login_info'])) {

        $loginModel = unserialize(serialize($_SESSION['login_info']));
        if ($loginModel->user_type == 'Admin') {
            return true;
        }
    }
    return false;
}

function checkAuthenticationToAction($action) {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    if (isset($_SESSION['login_info'])) {
        $loginModel = unserialize(serialize($_SESSION['login_info']));


        $userLevelModel = UserLevels::findOne($loginModel->user_level);


        $level_authenticationsString = $userLevelModel->level_authentications;
        if (strpos($level_authenticationsString, $action) !== false) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function getMyID() {
    $action = Yii::$app->urlManager->parseRequest(Yii::$app->request);

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    if (isset($_SESSION['login_info'])) {
        $loginModel = unserialize(serialize($_SESSION['login_info']));

        return $loginModel->id;
    }

    return '';
}

function checkAuthentication($this1) {

//    $action = Yii::$app->urlManager->parseRequest(Yii::$app->request);


    $action = Yii::$app->controller->id . "/" . Yii::$app->controller->action->id;

//    display_array($action);
//    exit;
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }


    if (isset($_SESSION['login_info'])) {


//    
//    var_dump($_SESSION);
//    exit;


        $loginModel = unserialize(serialize($_SESSION['login_info']));
        $loginModel = \app\models\Users::find()->where(['id' => $loginModel->id])->one();


//        display_array($action);
//        display_array($loginModel->user_level);
//        display_array(app\models\PageAuthentications::find()->all());
//        exit;
//        $pageAuthentications = app\models\PageAuthentications::find()->where(['level_id' => $loginModel->user_level])->all();
//        display_array($pageAuthentications);
//        exit;

        $pageAuthentications = app\models\PageAuthentications::find()->where(['level_id' => $loginModel->user_level, 'authentication' => $action])->one();

//        display_array($pageAuthentications);
//        exit;
        if ($pageAuthentications == null) {
            if (!isset($_POST['from_approvals'])) {
                $this1->redirect(['users/permission-denied']);
            }
        }

        if ($loginModel->changed_password_once == "") {
            $this1->redirect(['users/change-password']);
        }


//            $userLevelModel = UserLevels::findOne($loginModel->user_level);
//             
//            $level_authenticationsString = $userLevelModel->level_authentications;
//            if( strpos($level_authenticationsString, $action[0]) !== false )
//            {
//                
//            }
//            else 
//            {
//                $this1->redirect('index.php?r=users/permission-denied');
//            }
    } else {
        $this1->redirect('index.php?r=users/login&goback=true');
    }
}

function checkAuthenticationPage($page) {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    if (isset($_SESSION['login_info'])) {

        $loginModel = unserialize(serialize($_SESSION['login_info']));

//            $userLevelModel = UserLevels::findOne($loginModel->user_level);
//              $userLevelModel = UserLevels::findOne('2');


        $pageAuthentications = app\models\PageAuthentications::find()->where(['level_id' => $loginModel->user_level, 'authentication' => $page])->one();
        if ($pageAuthentications != null) {
            return true;
        }
    }

    return false;
}

function searchInArray1($array, $key, $value) {
    foreach ($array as $subarray) {
        if (isset($subarray[$key]) && $subarray[$key] == $value)
            return $subarray;
    }
}

function checkExistance($array, $key, $value) {
    $results = searchInArray($array, $key, $value);

    if (count($results) > 0) {
        if (isset($results[0]['value'])) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function searchInArray($array, $key, $value) {
    $results = array();

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }

        foreach ($array as $subarray) {
            $results = array_merge($results, searchInArray($subarray, $key, $value));
        }
    }

    return $results;
}

function unitConvertorFromDB($unit, $val) {


    $final = 0;
    $data = \app\models\Units::find()->where(['unit_name' => $unit])->asArray()->one();

    $final = $data['convert_rate_per_kg_and_lt'] * $val;


    return $final;
}

function fullDateToMMYYY($date) {
    $getMonthYear = "";
    if (isset($date) && $date != "") {
        $getDate = strftime("%d-%m-%Y", strtotime($date));
        $getFullDateObject = DateTime::createFromFormat('d-m-Y', $getDate);
        $getMonthYear = $getFullDateObject->format('F-Y');
    }
    return $getMonthYear;
}

function fullDateToMMDDYYYY($date) {
    $getDateMonthYear = "";
    if (isset($date) && $date != "") {
        $getDate = strftime("%d-%m-%Y", strtotime($date));
        $getFullDateObject = DateTime::createFromFormat('d-m-Y', $getDate);
        $getDateMonthYear = $getFullDateObject->format('d-m-Y');
    }
    return $getDateMonthYear;
}

function fullDateToYYYYMMDD($date) {
    $getDateMonthYear = "";
    if (isset($date) && $date != "") {
        $getDate = strftime("%d-%m-%Y", strtotime($date));
        $getFullDateObject = DateTime::createFromFormat('d-m-Y', $getDate);
        $getDateMonthYear = $getFullDateObject->format('Y-m-d');
    }
    return $getDateMonthYear;
}

function MMYYYTo_mY_IntMonthYear($date) {
    $getIntMonthYear = "";

    if (isset($date) && $date != "") {
        $getDate = strftime("%d-%m-%Y", strtotime($date));
        $getFullDateObject = DateTime::createFromFormat('d-m-Y', $getDate);
        $getIntMonth = $getFullDateObject->format('m');
        $getIntYear = $getFullDateObject->format('Y');
        $getIntMonthYear = $getIntMonth . '-' . $getIntYear;
    }
    return $getIntMonthYear;
}

function getNumberofDaysFromMonthYear($date) {
    $getValue = MMYYYTo_mY_IntMonthYear($date); //return month-year(June-2016 => 06-2016) 
    $d = split("-", $getValue);
    $number = cal_days_in_month(CAL_GREGORIAN, $d[0], $d[1]); //(PHP function) return number of day from month-year
    return $number;
}

function addtoString($existString, $newValue) {
    $parts = explode(',', $existString);
    $parts[] = $newValue;
    return implode(',', $parts);
}

function findAgeInYearMonth($DDMMYYYY) {//put date in the dd-mm-yyyy format
    $localtime = getdate();
    $today = $localtime['mday'] . "-" . $localtime['mon'] . "-" . $localtime['year'];
    $dob_a = explode("-", $DDMMYYYY);
    $today_a = explode("-", $today);
    $dob_d = $dob_a[0];
    $dob_m = $dob_a[1];
    $dob_y = $dob_a[2];
    $today_d = $today_a[0];
    $today_m = $today_a[1];
    $today_y = $today_a[2];
    $years = $today_y - $dob_y;
    $months = $today_m - $dob_m;
    if ($today_m . $today_d < $dob_m . $dob_d) {
        $years--;
        $months = 12 + $today_m - $dob_m;
    }
    if ($today_d < $dob_d) {
        $months--;
    }
    $firstMonths = array(1, 3, 5, 7, 8, 10, 12);
    $secondMonths = array(4, 6, 9, 11);
    $thirdMonths = array(2);
    if ($today_m - $dob_m == 1) {
        if (in_array($dob_m, $firstMonths)) {
            array_push($firstMonths, 0);
        } elseif (in_array($dob_m, $secondMonths)) {
            array_push($secondMonths, 0);
        } elseif (in_array($dob_m, $thirdMonths)) {
            array_push($thirdMonths, 0);
        }
    }
    $output = $years . " Years " . $months . " Months";
    return $output;
}

function findAgeInYearMonthNew($DDMMYYYY) {//put date in the dd-mm-yyyy format
    $date = new DateTime($DDMMYYYY);
// $today = new DateTime('00:00:00'); - use this for the current date
    $today = new DateTime(); // for testing purposes
    $diff = $today->diff($date);
//    printf('%d years, %d month', $diff->y, $diff->m);
//    $p = printf('%d years, %d month', $diff->y, $diff->m);
    $p = $diff->y . " years, " . $diff->m . " months";
    return $p;
}

function getCattleTypeByDob($DDMMYYYY, $cattle = "") {//put date in the dd-mm-yyyy format
    $date = new DateTime($DDMMYYYY);
// $today = new DateTime('00:00:00'); - use this for the current date
    $today = new DateTime(); // for testing purposes
    $diff = $today->diff($date);
    $month = $diff->m;
    $year = $diff->y;
    $p = "";

    if ($cattle === "Cow") {
        if ($year == 0 && ($month >= 0 && $month <= 6)) {
            $p = 'Calf';
        } elseif ($year < 2 && $month <= 11) {
            $p = 'Heifer';
        } elseif ($year == 2 && $month <= 6) {
            $p = 'Heifer';
        } elseif ($year >= 2 && ( $month > 0 || $month <= 11)) {
            $p = 'Cattle';
        } else {
            $p = 'None';
        }
    } else {
        if ($year == 0 && ($month >= 0 && $month <= 6)) {
            $p = 'Calf';
        } elseif ($year < 3 && $month <= 11) {
            $p = 'Heifer';
        } elseif ($year == 3 && $month <= 0) {
            $p = 'Heifer';
        } elseif ($year >= 3 && ( $month > 0 || $month <= 11)) {
            $p = 'Cattle';
        } else {
            $p = 'None';
        }
    }
//    $p =  printf('%d years, %d month %d day', $diff->y, $diff->m , $diff->d);
//    $ppp = $diff->y . " years, " . $diff->m . " months";
    return $p;
}

function colorDropdown() {
    $array = ['Red','Green','Pink','Firozi','Mehndi','Yellow'];
    return arrayToDoubleArray($array);
}

?>
