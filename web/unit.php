
<?php

// Convert area units

$data = <<<INFO
# Area Unit conversions

# define base units

1, square millimetres

# define each unit in relation to a previously defined one

1, square centimetres, 100, square millimetres
1, square metres, 10000, square centimetres
1, hectares, 10000, square metres
1, square kilometers, 100, hectares

1, square inches, 645.16, square millimetres
1, square feet, 144, square inches
1, square yards, 9, square feet
1, acres, 4840, square yards
1, square miles, 640, acres
1, townships, 36, square miles

10, square chains, 1, acres
160, square rods, 1, acres
INFO;

$optlist = "";
foreach (explode("\n",$data) as $line) {
        list($du,$dn,$bu,$bn) = explode(", ",$line);
        if (ereg('^[[:space:]]*#',$du)) continue;
        if (ereg('^[[:space:]]*$',$du)) continue;
        if (! $bu) {
                $utable[$dn] = $du;
                $ultbase = $dn;
        } else {
                $utable[$dn] = 1.0 * $utable[$bn] * $bu / $du;
        }
        $optlist .= "<option value='$dn'>$dn";
        # print $line."<br>";
        # print "defining $dn as $utable[$dn] $ultbase<br>";
        }
$ulist = implode(", ",array_keys($utable));
if ($invert = $utable[$_REQUEST[units]]) {
        $result = "<em>$_REQUEST[amount] $_REQUEST[units] converts to</em><br>";
        $base = 1.0 * $_REQUEST[amount] * $invert;
        foreach (array_keys($utable) as $uname) {
                $newval = $base / $utable[$uname];
                if ($newval > 0.0001 and $newval < 10000.0)
                        $result .= "$newval $uname<br>";
        }
} else {
        $result = "Results appear here when you submit the form<br>";
}
?>
<html>
<head><title>Area Unit Conversion</title></head>
<body><h1>Area Conversions</h1>
<form>Please convert <input name=amount>
<select name=units> <?= $optlist ?> </select>
<input type=submit value=now> to alternative units</form>
<hr>
<?= $result ?>
<hr>
This program knows about the following units: <?= $ulist ?>. It only report
on reasonable alternative units - it won't report conversions that result in
very tiny or very large numeric values.
<hr>
 
<?= highlight_file($_SERVER[PATH_TRANSLATED]); ?>
<hr>
</body>
</html>
